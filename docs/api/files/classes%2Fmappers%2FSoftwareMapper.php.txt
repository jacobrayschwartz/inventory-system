<?php 
	/**
	* A class file for SoftwareMapper
	*/

	
	/**
	* SoftwareMapper Class
	* Maps a Software Asset to the database (or vice versa)
	* Implements I_DataMapper
	* Singleton
	*/
	class SoftwareMapper implements I_DataMapper{
		/**
		* The mysqli connection
		* @var mysqli
		*/
		private $db;

		/**
		* The instance of SoftwareMapper (Singleton Pattern)
		* @var SoftwareMapper
		*/
		private static $instance = null;

		/**
		* ModelFactory to build a computer
		* @var ModelFactory
		*/
		private $model_factory;

		/**
		* Private constructor for the SoftwareMapper, since  we are using the Singleton Pattern
		* @param mysqli $db
		*/
		private function __construct(mysqli $db){
			$this->db = $db;
			$this->model_factory = ModelFactory::GetInstance();
		}

		/**
		* Returns an instance of the SoftwareMapper, or creates a new one if it hasen't been instantiated yet
		* @param mysqli $db
		*/
		public static function GetInstance(mysqli $db){
			if(SoftwareMapper::$instance === null){
				if($db == null){
					throw new DBException($GLOBALS['debug'] ? "Database can't be null!" : $GLOBALS['general_error_message']);
				}
				SoftwareMapper::$instance = new SoftwareMapper($db);
			}

			return SoftwareMapper::$instance;
		}

		/**
		* Making sure this object can't be cloned
		*/
		private function __clone(){
			//Making sure the object cant be cloned
		}

		/**
		* Making sure this object can't be unserialized
		*/
		private function __wakeup(){
			//Making sure the object can't be unserialized
		}


		//===================== CRUD!

		/**
		* Updates the mysql table-row representing the Software passed in
		* I would like to eventually use a stored procedure here, but it may couple the database and mapper too much
		* @param Software $software
		* @return boolean if update was successful
		*/
		public function update($software){
			//Asset
			$id = $software->getDBID();
			$name = $software->getName();
			$parent_id = $software->getParentID();
			$person_id = $software->getPersonID();
			$location_id = $software->getLocationID();
			$asset_tag = $software->getAssetTag();
			$notes = $software->getNotes();
			//Software
			$serial = $software->getSerial();
			$version = $software->getVersion();
			$expiration_date = $software->getExpirationDate();



			$asset_stmt = $this->db->prepare("UPDATE assets SET name=?, parent_id=?, person_id=?, location_id=?, asset_tag=?, notes=? WHERE id=?");
			$software_stmt = $this->db->prepare("UPDATE software SET serial_code=?, version=?, expiration_date=?  WHERE asset_id=?");

			$asset_stmt->bind_param('siiisss', $name, $parent_id, $person_id, $location_id, $asset_tag, $notes, $id);
			if(!$asset_stmt->execute()){
				throw new DBException('Error Executing Statement!<br>' . $this->db->error);
			}
			unset($asset_stmt);

			$software_stmt->bind_param('ssss', $serial, $version, $expiration_date, $id);
			$software_stmt->execute();
			if(!$software_stmt->execute()){
				throw new DBException('Error Executing Statement!<br>' . $this->db->error);
			}
			unset($software_stmt);

			return true;
		}

		/**
		* Creates the mysql table-row that will represent the data passed in
		* @param Software $software
		* @return boolean if creation was successful
		*/
		public function create($software){
			//Asset
			$id = $software->getDBID();
			$name = $software->getName();
			$parent_id = $software->getParentID();
			$person_id = $software->getPersonID();
			$location_id = $software->getLocationID();
			$asset_tag = $software->getAssetTag();
			$notes = $software->getNotes();
			//Software
			$serial = $software->getSerial();
			$version = $software->getVersion();
			$expiration_date = $software->getExpirationDate();


			$asset_stmt = $this->db->prepare("INSERT INTO assets SET name=?, parent_id=?, person_id=?, location_id=?, asset_tag=?, notes=?, id=?");
			

			$asset_stmt->bind_param('siiisss', $name, $parent_id, $person_id, $location_id, $asset_tag, $notes, $id);
			if(!$asset_stmt->execute()){
				throw new DBException('Error Executing Statement!<br>' . $this->db->error);
			}
			unset($asset_stmt);

			$software_stmt = $this->db->prepare("INSERT INTO software SET serial_code=?, version=?, expiration_date=?, asset_id=?");
			$software_stmt->bind_param('ssss', $serial, $version, $expiration_date, $id);
			if(!$software_stmt->execute()){
				throw new DBException('Error Executing Statement!<br>' . $this->db->error);
			}
			unset($software_stmt);

			return true;
		}


		/**
		* Returns all software in the database
		* @return array $software
		* @throws DBException
		*/
		public function findAll(){
			$software = array();

			$rows = $this->db->query("SELECT id AS 'dbid', name, parent_id, person_id, location_id, asset_tag, notes, serial_code as 'serial', model_type, version, expiration_date FROM software LEFT JOIN assets ON software.asset_id=assets.id");
			if(!$rows){
				//If nothing was returned
				throw new DBException("DBException: " . $this->db->error);
			}
			else{
				while($row = $rows->fetch_assoc()){
					$software[] = $this->model_factory->generateModel($row);
				}
			}
			return $software;
		}


		/**
		* Looks for the software in the database that match the ID's passed in
		* @param array $ids
		* @return array $software
		* @throws ValidationException
		* @throws DBException
		*/
		public function find(array $ids){
			$software = array();
			$id_string = '';
			for ($i = 0; $i < count($ids); $i++) {
				//Because I don't know how long the list of ids is going to be, I can't use a prepared statement, but the regex for a DBID should guard against SQL injection
				if(!preg_match(Model::$regex_dbid, $ids[$i])){
					throw new ValidationException('There was an issue with finding that model.','Invalid DBID: ' . $ids[$i]);
				}
				$id_string .= "'" . $ids[$i] . "'";
				if($i < count($ids) - 1){
					$id_string .= ', ';
				}
			}


			$rows = $this->db->query("SELECT id AS 'dbid', name, parent_id, person_id, location_id, asset_tag, notes, serial_code as 'serial', model_type, version, expiration_date FROM software LEFT JOIN assets ON software.asset_id=assets.id WHERE id IN ($id_string)");
			if(!$rows){
				//If nothing was returned
				throw new DBException("DBException: " . $this->db->error);
			}
			else if($rows->num_rows > 0){
				while($row = $rows->fetch_assoc()){
					$software[] = $this->model_factory->generateModel($row);
				}
			}
			return $software;
		}

		/**
		* Deletes the row in the mysql database representing the software
		* @param int $id
		* @return boolean if deletion was successful
		* @throws ValidationException
		* @throws DBException
		*/
		public function delete($id){
			if(!preg_match(Model::$regex_dbid, $id)){
					throw new ValidationException('There was an issue with finding that model.','Invalid DBID: ' . $id);
			}

			//Because I used foreign keys to set the database, we only have to delete the asset record, the rest is taken care of by the database
			$stmt = $this->db->prepare("DELETE FROM assets WHERE id=?");
			$stmt->bind_param('s', $id);
			if(!$stmt->execute()){
				throw new DBException('Couldnt execute stmt: ' . $this->db->error());
			}
			$num = $stmt->affected_rows;
			unset($stmt);
			return ($num > 0);
		}
	}
?>
