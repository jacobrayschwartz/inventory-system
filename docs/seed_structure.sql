-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 03, 2014 at 08:51 PM
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `inventory`
--
CREATE DATABASE IF NOT EXISTS `inventory` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `inventory`;

-- --------------------------------------------------------

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
CREATE TABLE IF NOT EXISTS `assets` (
  `id` char(48) NOT NULL COMMENT 'UUID generated from the application',
  `name` varchar(255) NOT NULL COMMENT 'Display Name',
  `parent_id` char(48) DEFAULT NULL COMMENT 'DBID of parent asset (the computer that a monitor belongs to)',
  `person_id` char(48) DEFAULT NULL COMMENT 'DBID of this asset''s user',
  `location_id` char(48) DEFAULT NULL COMMENT 'DBID of this asset''s location',
  `asset_tag` varchar(255) DEFAULT NULL COMMENT 'Company asset tag',
  `notes` text,
  `model_type` varchar(255) NOT NULL,
  `line_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'This is to help the database access. Because using a UUID as the primary key negatively impacts performance, it''s suggested that a second if with auto increment should be used. THIS IS NOT MADE FOR USE IN QUERIES',
  PRIMARY KEY (`line_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_location_asset_idx` (`location_id`),
  KEY `fk_parent_asset_idx` (`person_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `computers`
--

DROP TABLE IF EXISTS `computers`;
CREATE TABLE IF NOT EXISTS `computers` (
  `asset_id` char(48) NOT NULL,
  `disk_space` int(11) NOT NULL,
  `memory` int(11) NOT NULL,
  `processor` varchar(255) NOT NULL,
  `os` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `line_id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`line_id`),
  UNIQUE KEY `asset_id_UNIQUE` (`asset_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Table for the computer assets';

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE IF NOT EXISTS `locations` (
  `id` char(48) NOT NULL,
  `building` varchar(20) NOT NULL,
  `number` int(11) NOT NULL,
  `model_type` varchar(30) NOT NULL DEFAULT 'location',
  `line_id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`line_id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

DROP TABLE IF EXISTS `people`;
CREATE TABLE IF NOT EXISTS `people` (
  `id` char(48) NOT NULL,
  `employee_id` varchar(255) DEFAULT NULL,
  `first` varchar(50) NOT NULL COMMENT 'First Name',
  `last` varchar(50) NOT NULL COMMENT 'Last Name',
  `email` varchar(100) DEFAULT NULL COMMENT 'Person''s Email',
  `phone` int(11) DEFAULT NULL COMMENT 'Person''s Phone',
  `ext` int(11) DEFAULT NULL COMMENT 'Person''s Extention for the phone (Optional)',
  `model_type` varchar(30) NOT NULL DEFAULT 'person' COMMENT 'Should always be person (for consistency in the code)',
  `line_id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`line_id`),
  UNIQUE KEY `dbid` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Stores the information of people for assigning them to assets';

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Database ID for the permission',
  `permission` varchar(64) NOT NULL COMMENT 'Name of the permission',
  `comments` varchar(255) DEFAULT NULL COMMENT 'Comments on the permission',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Various permissions for inventory users';

-- --------------------------------------------------------

--
-- Table structure for table `software`
--

DROP TABLE IF EXISTS `software`;
CREATE TABLE IF NOT EXISTS `software` (
  `asset_id` char(48) NOT NULL,
  `serial_code` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `line_id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`line_id`),
  UNIQUE KEY `asset_id` (`asset_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Database id for the user',
  `username` varchar(64) NOT NULL COMMENT 'To log in',
  `pass` varchar(64) DEFAULT NULL COMMENT 'password (should be created and checked with PHPASS',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Users that can access the inventory system';

-- --------------------------------------------------------

--
-- Table structure for table `user_permissions`
--

DROP TABLE IF EXISTS `user_permissions`;
CREATE TABLE IF NOT EXISTS `user_permissions` (
  `user_id` int(11) NOT NULL COMMENT 'Database ID for the user',
  `permission_id` int(11) NOT NULL COMMENT 'Database ID for the permission',
  PRIMARY KEY (`user_id`,`permission_id`),
  KEY `fk_user_permissions_permissions_idx` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Association table for users and permissions';

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assets`
--
ALTER TABLE `assets`
  ADD CONSTRAINT `fk_location_asset` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_parent_asset` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_person_asset` FOREIGN KEY (`person_id`) REFERENCES `people` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `computers`
--
ALTER TABLE `computers`
  ADD CONSTRAINT `fk_computer_asset` FOREIGN KEY (`asset_id`) REFERENCES `assets` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `software`
--
ALTER TABLE `software`
  ADD CONSTRAINT `fk_software_assets` FOREIGN KEY (`asset_id`) REFERENCES `assets` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_permissions`
--
ALTER TABLE `user_permissions`
  ADD CONSTRAINT `fk_user_permissions_permissions` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_permissions_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
