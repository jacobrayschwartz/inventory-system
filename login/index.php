<?php
	//The login page for the site
	//If a user is logged in and comes to this page, they should be immediately logged out
	require_once '../utils/__config.php';
	$_SESSION['user_id'] = '';
	unset($_SESSION['user_id']);
	$_SESSION['user_username'] = '';
	unset($_SESSION['user_username']);
?>


<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo $webroot ?>/styles/main.css">
	<title>Log in to Inventory</title>
</head>
<body>
	<nav class='navbar-default navbar-default' role='navigation'>
		<div class='container-fluid'>
			<div class='navbar-header'>
				<div class='navbar-brand'>Inventory</div>
			</div><!-- end .navbar-header -->
		</div> <!-- end .container -->
	</nav> <!-- end nav -->

	<aside class='content'>
			<div style='margin-top: 25px;'></div>  <!-- Spacer -->
		<div class='container col-md-4 col-md-offset-4'>
			<div class='panel panel-primary' id='panel-login'>
				<div class='panel-heading'>
					<h3 class='panel-title'>Log In</h3>
				</div> <!-- end .panel-heading -->
				<div class='panel-body'>
					<div class='alert alert-danger error' role='alert'></div>
					<form id='login-form'>
						<div class='form-group'>
							<div class='input-group'>
								<div class='input-group-addon'><div class='fa fa-user'></div></div>
								<input type='text' class='form-control' name='username' placeholder='Username'>
							</div><!-- end .input-group username -->
						</div><!-- end .form-group username -->


						<div class='form-group'>
							<div class='input-group'>
								<div class='input-group-addon'><div class='fa fa-lock'></div></div>
								<input type='password' class='form-control' name='password' placeholder='Password'>
							</div><!-- end .input-group password -->
						</div><!-- end .form-group password -->
						
						<button class='btn btn-primary submit col-md-12' role='submit'>Log In</button>

					</form>
				</div><!-- end .panel-body -->
			</div><!-- end #panel-login -->
		</div> <!-- end .container -->
		
	</aside>


	


	<script src='https://code.jquery.com/jquery-2.1.1.min.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/affix.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/alert.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/button.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/carousel.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/popover.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/tab.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js'></script>


	<script type="text/javascript">
	//Initialization
	$(document).ready(function(){
		$('.alert').hide();
	});



	//Log in button
	$('#panel-login').on('click','.submit',function(e){
		e.preventDefault();
		$('#panel-login .error').slideUp();

		var data = $('#login-form').serialize();

		//Sending the login data
		$.post('process_login.php',
			data,
			function(response){
				if(response.problem){ //If there was an issue logging in
					$('#panel-login .error').html(response.message).slideDown();
				}
				else{
					window.location = '/inventory';
				}
			},
			'json');
	});
	</script>
</body>


</html>