<?php
	/**
	* Processing the user login
	* Returns JSON
	* * problem {true|false} (bad credentials/invalid chars etc)
	* * message string
	* * Error message if problem=true
	* If the user is validated, the appropriate session variables are set
	*/

	require_once '../utils/__config.php';

	//If a user is logged in and comes to this page, they should be immediately logged out before processing
	$_SESSION['user_id'] = '';
	unset($_SESSION['user_id']);
	$_SESSION['user_username'] = '';
	unset($_SESSION['user_username']);


	$problem = false; //For json
	$message = ''; //For json
	$in_username = isset($_POST['username']) ? $_POST['username'] : ''; //Username coming from form
	$in_pass = isset($_POST['password']) ? $_POST['password'] : ''; //Password coming from form
	$user_id = 0; //ID of user in the database 
	$hash = ''; //Pasword from database
	$db = null;
	$ph = null;


	try {
		$db = getDBConn(); //Getting the mysqli object
		$ph = getHasher();

		if(!preg_match($GLOBALS['regex_username'], $in_username)){
			throw new ValidationException("The username should contain letters, numbers and -'s only!", "VALIDATION ERROR - Regex - The username should contain letters, numbers and -'s only!");
		}

		$stmt = $db->prepare("SELECT id, pass FROM users WHERE username=?");
		$stmt->bind_param('s',$in_username);
		if(!$stmt->execute()){
			throw new DBException('DATABASE ERROR - ' . $db->error);
		}
		//Grabbing data
		$stmt->store_result();
		$stmt->bind_result($user_id, $hash);
		$stmt->fetch();

		if($stmt->num_rows != 1){
			throw new ValidationException("We're sorry, there is no user with those credentials.", "VALIDATION ERROR - No user");
		}

		//Checking password
		if(!$ph->CheckPassword($in_pass,$hash)){
			throw new ValidationException("We're sorry, there is no user with those credentials.", "VALIDATION ERROR - User/Password missmatch");
		}


		//If we got here, the user is ok
		$_SESSION['user_id'] = $user_id;
		$_SESSION['user_username'] = $in_username;

	} catch(ValidationException $e){
		//Validation issue (bad chars, invalid credentials, etc)
		$problem = true;
		$message = ($GLOBALS['debug'] == true) ? $e->getMessage() : $e->getUserMessage();
	} catch(Exception $e){
		//Some unexpected issue
		$problem = 'true';
		if($GLOBALS['debug'] == true){
			$message = "EXCEPTION - " . $e->getMessage();
		}
		else{
			$message = $GLOBALS['general_error_message'];
		}
	}

	//Closing the database
	$db->close();//Closing the DB connection

	//Destroying the hasher
	unset($ph);

	//Finally, echoing the json
	$json = array('problem' => $problem, 'message' => $message);
	echo json_encode($json);
	

	
?>