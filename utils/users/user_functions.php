<?php
	/**
	* Useful functions for users
	*/

	/**
	* Function for validating the currently logged in user
	* @return boolean
	*/
	function validateCurrentUser(){
		//Going to start assuming the user is not valid (guilty until proven innocent)
		$valid = false;
		$db = getDBConn();

		if(isset($_SESSION['user_id']) && isset($_SESSION['user_username'])){
			$user_id = $_SESSION['user_id'];
			$username = $_SESSION['user_username'];
			if(preg_match($GLOBALS['regex_user_id'], $user_id) && preg_match($GLOBALS['regex_username'], $username)){
				$stmt = $db->prepare("SELECT id, username FROM users WHERE id=? AND username=?");
				$stmt->bind_param('is',$user_id, $username);
				$stmt->execute();
				$stmt->store_result();
				if($stmt->num_rows == 1){
					$valid = true;
				}
				$stmt->free_result();
			}
		}

		$db->close();
		return $valid;
	}


	/**
	* Function for getting the permissions of the user
	* @param int $user_id DBID for the user in question
	* @return array
	*/
	function getUserPermissions($user_id = NULL){
		//If no specific user ID was passed in, default to current user
		if($user_id == NULL){
			$user_id = $_SESSION['user_id'];
		}
		
		$permissions = array();
		$db = getDBConn();
		$permission; //Holder for bind_result

		if(!preg_match($GLOBALS['regex_user_id'], $user_id)){
			throw new Exception('Invalid user id!');
		}

		$stmt = $db->prepare("SELECT permission FROM user_permissions LEFT JOIN permissions ON permissions.id=user_permissions.permission_id WHERE user_id=?");
		$stmt->bind_param('i',$user_id);
		if(!$stmt->execute()){
			throw new DBException('Could not execute query!' . (isset($GLOBALS['debug']) && $GLOBALS['debug'] === true ? '<br/><strong>' . $db->error . '</strong>' : ''));
		}

		$stmt->bind_result($permission);
		while($stmt->fetch()){
			$permissions[] = $permission;
		}
		$stmt->free_result();

		$db->close();
		return $permissions;
	}
?>