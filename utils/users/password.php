<?php
	/**
	* Configure and create a PasswordHasher object
	*/

	/**
	* Return a password hasher
	* @return PasswordHash
	*/
	function getHasher(){
		$log_2 = $GLOBALS['phpass_log2'];
		$portable = $GLOBALS['phpass_portable'];

		return new PasswordHash($log_2, $portable);
	}
?>