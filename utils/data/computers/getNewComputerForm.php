<?php

require_once '../../__config.php';

$error = false; //If there was a problem
$data = ''; //Form returned, or error message

//If the user isn't valid, send an error back
if(!validateCurrentUser()){
	$error = true;
	$data = "You have to be logged in to do that...";
}


if(!$error){
	$data = Computer::GetNewHTML();
}



//Finally echo if there is an error and the data in JSON
echo json_encode(array('error' => $error, 'data' => $data));

?>