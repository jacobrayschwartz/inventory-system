<?php
/**
* Adding or updating a computer in the database
*/

require_once '../../__config.php';

$error = false; //If there was a problem
$data = ''; //Models returned, or error message
//If the current user isn't valid, send an error back
if(!validateCurrentUser()){
	$error = true;
	$data = "You have to be logged in to do that...";
}

//Grabbing the permissions of the current user
$cur_permissions = getUserPermissions();

//If the user is not allowed to do this
if(count(array_intersect($cur_permissions, array('admin', 'manage'))) < 1){
	$error = true;
	$data = "You do not have permission to do that...";
}

$db = getDBConn();

if(!$error){
	try{
		//Grabbing all of the form data
		$dbid = (isset($_POST['id']) && !empty($_POST['id'])) ? $_POST['id'] : null;
		$name = (isset($_POST['name']) && !empty($_POST['name'])) ? $_POST['name'] : null;
		$asset_tag = (isset($_POST['asset_tag']) && !empty($_POST['asset_tag'])) ? $_POST['asset_tag'] : null;
		$employee_id = (isset($_POST['employee_id']) && !empty($_POST['employee_id'])) ? $_POST['employee_id'] : null;
		$location_name = (isset($_POST['location']) && !empty($_POST['location'])) ? $_POST['location'] : null;
		$disk_space = (isset($_POST['disk_space']) && !empty($_POST['disk_space'])) ? $_POST['disk_space'] : null;
		$memory = (isset($_POST['memory']) && !empty($_POST['memory'])) ? $_POST['memory'] : null;
		$os = (isset($_POST['os']) && !empty($_POST['os'])) ? $_POST['os'] : null;
		$processor = (isset($_POST['processor']) && !empty($_POST['processor'])) ? $_POST['processor'] : null;
		$type = (isset($_POST['type']) && !empty($_POST['type'])) ? $_POST['type'] : null;
		$notes = (isset($_POST['notes']) && !empty($_POST['notes'])) ? $_POST['notes'] : null;

		//Set later
		$person_id = null;
		$location_id = null;

		//First, we want to find the person by their employee id
		if($employee_id != null){
			$personMapper = PersonMapper::GetInstance($db);
			$person_arr = $personMapper->findByEmployeeID(array($employee_id));
			if(isset($person_arr[0])){
				$person_id = $person_arr[0]->getDBID();
				unset($person_arr[0]);
			}
			else{
				//If that person was not found, we should inform the user.
				throw new RequiredModelException('person', "Referenced person not found! - Employee ID: $employee_id");
			}
		}

		//Now looking for the location ID
		if($location_name != null){
			$locationMapper = LocationMapper::GetInstance($db);
			$location_arr = $locationMapper->findByName(array($location_name));
			if(isset($location_arr[0])){
				$location_id = $location_arr[0]->getDBID();
				unset($location_arr[0]);
			}
			else{
				//If no location was found
				throw new RequiredModelException('location', 'Referenced location not found! - Name:' . $location_name);
			}
		}

		//Now that we have the data, update the model or create a new one
		$mapper = ComputerMapper::GetInstance($db);
		$computer = new Computer($name, null, $person_id, $location_id, $asset_tag, $disk_space, $memory, $processor, $os, $type, $notes, $dbid);

		//Creating a new Computer
		if($dbid  === null){
			$mapper->create($computer);
		}
		//Updating an existing Computer
		else{
			$mapper->update($computer);
		}

		unset($computer);

		//If we're here, the update/create completed successfully
		$data = 'Computers updated successfully!';

	}
	catch(ValidationException $e){
		$error = true;
		if($GLOBALS['debug']){
			$data = $e->getMessage();
		}
		else{
			$data = $e->getUserMessage();
		}
	}
	catch(RequiredModelException $e){
		//One of the models referenced does not exist
		$error = true;
		if($GLOBALS['debug']){
			$data = $e->getMessage();
		}
		else{
			$data = $e->getUserMessage();
		}
	}
	catch(DBException $e){
		$error = true;
		if($GLOBALS['debug']){
			$data = $e->getMessage();
		}
		else{
			$data = 'There was an issue with that request. Please try again.';
		}
	}
}




if(isset($db)){
	$db->close();
	unset($db);
}

echo json_encode(array('data' => $data, 'error' => $error))
?>