<?php
/**
* Adding or updating a location in the database
*/

require_once '../../__config.php';

$error = false; //If there was a problem
$data = ''; //Models returned, or error message
//If the current user isn't valid, send an error back
if(!validateCurrentUser()){
	$error = true;
	$data = "You have to be logged in to do that...";
}

//Grabbing the permissions of the current user
$cur_permissions = getUserPermissions();

//If the user is not allowed to do this
if(count(array_intersect($cur_permissions, array('admin', 'manage'))) < 1){
	$error = true;
	$data = "You do not have permission to do that...";
}

$db = getDBConn();

if(!$error){
	try{
		//Grabbing all of the form data
		$dbid = (isset($_POST['id']) && !empty($_POST['id'])) ? $_POST['id'] : null;
		$building = (isset($_POST['building']) && !empty($_POST['building'])) ? $_POST['building'] : null;
		$number = (isset($_POST['number']) && !empty($_POST['number'])) ? $_POST['number'] : null;

		//Now that we have the data, update the model or create a new one
		$mapper = LocationMapper::GetInstance($db);
		$location = new Location($dbid, $building, $number);

		//Creating a new Location
		if($dbid  === null){
			$mapper->create($location);
		}
		//Updating an existing Location
		else{
			$mapper->update($location);
		}

		unset($location);

		//If we're here, the update/create completed successfully
		$data = 'Locations updated successfully!';

	}
	catch(ValidationException $e){
		$error = true;
		if($GLOBALS['debug']){
			$data = $e->getMessage();
		}
		else{
			$data = $e->getUserMessage();
		}
	}
	catch(RequiredModelException $e){
		//One of the models referenced does not exist
		$error = true;
		if($GLOBALS['debug']){
			$data = $e->getMessage();
		}
		else{
			$data = $e->getUserMessage();
		}
	}
	catch(DBException $e){
		$error = true;
		if($GLOBALS['debug']){
			$data = $e->getMessage();
		}
		else{
			$data = 'There was an issue with that request. Please try again.';
		}
	}
}




if(isset($db)){
	$db->close();
	unset($db);
}

echo json_encode(array('data' => $data, 'error' => $error))
?>