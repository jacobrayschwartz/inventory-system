<?php
/**
* This script is to be called by the client side javascript, it will pull the requested models from the database
* It will return a 2D array in JSON form, object's data including their types
* Unfortunately, this script will have to run through any tables that contain model data, which will require several queries
* Might want to change it to a stored procedure if possible
*/

require_once '../__config.php';

$error = false; //If there was a problem
$data = ''; //Models returned, or error message
$models = array(); //array to store the models
$computers = null; //The model objects returned
$ids = (isset($_POST['ids'])) ? $_POST['ids'] : null;
$genHTML = isset($_POST['html']) && $_POST['html'] == true; //If the html for the models should be returned (this will greatly increase the amount of data sent back and should only be used on a small number of models at a time)
$db = null;
$factory = null;

//If the current user isn't valid, send an error back
if(!validateCurrentUser()){
	$error = true;
	$data = "You have to be logged in to do that...";
}


try{
	//Setting up the database connection
	$db = getDBConn();

	/*
	Validating the ids so we can use them witout a prepared statement
	Allowes us to use WHERE dbid IN (...)
	*/
	$id_string = '';
	for ($i = 0; $i < count($ids); $i++) {
		if(!preg_match(Model::$regex_dbid, $ids[$i])){
			throw new ValidationException('There was an issue with finding that model.','Invalid DBID: ' . $ids[$i]);
		}
		$id_string .= "'" . $ids[$i] . "'";
		if($i < count($ids) - 1){
			$id_string .= ', ';
		}
	}


	//Getting the model factory
	$factory = ModelFactory::GetInstance();

	$query_strings = array();
	//Computers
	$query_strings[] = "SELECT assets.id as dbid, name, parent_id, person_id, location_id, asset_tag, disk_space, memory, os, processor, type, model_type, notes FROM computers LEFT JOIN assets ON computers.asset_id=assets.id WHERE assets.id IN ($id_string)";
	//Software
	$query_strings[] = "SELECT assets.id as dbid, name, parent_id, person_id, location_id, asset_tag, version, serial_code, expiration_date, model_type, notes FROM software LEFT JOIN assets ON software.asset_id=assets.id WHERE assets.id IN ($id_string)";
	//People (employees)
	$query_strings[] = "SELECT id as dbid, first, last, email, phone, ext, model_type, employee_id FROM people WHERE id IN ($id_string)";
	//Locations
	$query_strings[] = "SELECT id as dbid, building, `number`, model_type FROM locations WHERE id IN ($id_string)";

	foreach ($query_strings as $key => $query_string) {
		if(!$rows = $db->query($query_string)){
			throw new DBException("Couldn't pull models from DB!<br>\n" . $db->error . "<br>\nQuery: $query_string");
		}
		while($row = $rows->fetch_assoc()){
			$model = $factory->generateModel($row);
			$arr = $model->getInfoArray();
			//If we want to include the HTML
			if($genHTML){
				$arr['html'] = $model->getHTML();
			}
			$models[] = $arr;
		}
	}

	//Assuming there were no errors, set data (return value) as the models
	$data = $models;


}
catch(ValidationException $e){
	$error = true;
	$data = ($GLOBALS['debug'] == true) ? $e->getMessage() : $e->getUserMessage();
}
catch(DBException $e){
	//Some unexpected database issue
	$error = 'true';
	if($GLOBALS['debug'] == true){
		$data = "EXCEPTION - " . $e->getMessage();
	}
	else{
		$data = $GLOBALS['general_error_message'];
	}
}
catch(Exception $e){
	//Some unexpected issue
	$error = 'true';
	if($GLOBALS['debug'] == true){
		$data = "EXCEPTION - " . $e->getMessage();
	}
	else{
		$data = $GLOBALS['general_error_message'];
	}
}


echo json_encode(array('data' => $data, 'error' => $error));


?>