<?php
/**
* 
*/
require_once '../../__config.php';

$error = false; //If there was a problem
$data = ''; //Models returned, or error message
//If the current user isn't valid, send an error back
if(!validateCurrentUser()){
	$error = true;
	$data = "You have to be logged in to do that...";
}




$db = getDBConn();


if(!$error){
	$loanController = LoanController::GetInstance($db);
	$data = $loanController->getAllAssetsCheckedOut();
}


$db->close();

echo(json_encode(array('error' => $error, 'data' => $data)));
?>