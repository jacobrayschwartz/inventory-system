<?php
	/**
	* This script is to be called by the client side JavaScript
	* It will pull the requested persons from the database
	*/

	require_once '../../__config.php';

	$error = false; //If there was a problem
	$data = ''; //People returned, or error message
	$persons = null; //The people objects, temporary
	$ids = (isset($_POST['ids'])) ? $_POST['ids'] : null;

	//If the user isn't valid, send an error back
	if(!validateCurrentUser()){
		$error = true;
		$data = "You have to be logged in to do that...";
	}

	//Pulling the data
	if(!$error){
		try{
			//Getting the DB connection
			$db = getDBConn();
			//Getting the mapper
			$personMapper = PersonMapper::GetInstance($db);
			if($ids === null){
				//Looking for all persons
				$persons = $personMapper->findAll();
			}
			else{
				//Looking for specific persons
				$persons = $personMapper->find($ids);
			}
			$data = array();
			for($i = 0; $i < count($persons); $i++){
				$data[] = $persons[$i]->getInfoArray();
			}
		}
		catch(DBException $e){
			$error = true;
			if($GLOBALS['debug']){
				$data = $e->getMessage();
			}
			else{
				$data = 'There was an issue with that request. Please try again.';
			}
		}
		catch(ValidationException $e){
			$error = true;
			if($GLOBALS['debug']){
				$data = "VALIDATION EXCEPTION: getPeople: " . $e->getMessage();
			}
			else{
				$data = "There was some invalid data there! <br>" . $e->getUserMessage();
			}
		}
		catch(Exception $e){
			$error = true;
			if($GLOBALS['debug']){
				$data = $e->getMessage();
			}
			else{
				$data = 'Something went wrong. Try again later.';
			}
		}
	}


	//Finally echo if there is an error and the data in JSON
	echo json_encode(array('error' => $error, 'data' => $data));
?>