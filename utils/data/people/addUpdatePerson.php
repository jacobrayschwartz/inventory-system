<?php
/**
* Adding or updating a person in the database
*/

require_once '../../__config.php';

$error = false; //If there was a problem
$data = ''; //Models returned, or error message
//If the current user isn't valid, send an error back
if(!validateCurrentUser()){
	$error = true;
	$data = "You have to be logged in to do that...";
}

//Grabbing the permissions of the current user
$cur_permissions = getUserPermissions();

//If the user is not allowed to do this
if(count(array_intersect($cur_permissions, array('admin', 'manage'))) < 1){
	$error = true;
	$data = "You do not have permission to do that...";
}

$db = getDBConn();

if(!$error){
	try{
		//Grabbing all of the form data
		$dbid = (isset($_POST['id']) && !empty($_POST['id'])) ? $_POST['id'] : null;
		$first = (isset($_POST['first']) && !empty($_POST['first'])) ? $_POST['first'] : null;
		$last = (isset($_POST['last']) && !empty($_POST['last'])) ? $_POST['last'] : null;
		$employee_id = (isset($_POST['employee_id']) && !empty($_POST['employee_id'])) ? $_POST['employee_id'] : null;
		$email = (isset($_POST['email']) && !empty($_POST['email'])) ? $_POST['email'] : null;
		$phone = (isset($_POST['phone']) && !empty($_POST['phone'])) ? $_POST['phone'] : null;
		$ext = (isset($_POST['ext']) && !empty($_POST['ext'])) ? $_POST['ext'] : null;


		//Now that we have the data, update the model or create a new one
		$mapper = PersonMapper::GetInstance($db);
		$person = new Person($employee_id, $first, $last, $email, $phone, $ext, $dbid);

		//Creating a new Person
		if($dbid  === null){
			$mapper->create($person);
		}
		//Updating an existing Person
		else{
			$mapper->update($person);
		}

		unset($person);

		//If we're here, the update/create completed successfully
		$data = 'People updated successfully!';

	}
	catch(ValidationException $e){
		$error = true;
		if($GLOBALS['debug']){
			$data = $e->getMessage();
		}
		else{
			$data = $e->getUserMessage();
		}
	}
	catch(RequiredModelException $e){
		//One of the models referenced does not exist
		$error = true;
		if($GLOBALS['debug']){
			$data = $e->getMessage();
		}
		else{
			$data = $e->getUserMessage();
		}
	}
	catch(DBException $e){
		$error = true;
		if($GLOBALS['debug']){
			$data = $e->getMessage();
		}
		else{
			$data = 'There was an issue with that request. Please try again.';
		}
	}
}




if(isset($db)){
	$db->close();
	unset($db);
}

echo json_encode(array('data' => $data, 'error' => $error))
?>