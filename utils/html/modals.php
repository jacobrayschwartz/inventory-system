<?php
	//This file stores the various modals (bootstrap) that we want to have in the main site.
	//Just use include ''; to add them
?>

<!-- 
	This modal is to display the details of a model
 -->
<div class="modal fade model details" id='details-modal'>
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Details</h4>
      </div>
      <div class="modal-body">
      	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary value show-edit">Edit</button><button type="button" class="btn btn-warning edit cancel-edit">Cancel</button>
        <button type="button" class="btn btn-default value" data-dismiss="modal">Close</button><button type="button" class="btn btn-success edit submit-form">Submit</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- 
  This modal is to display the "New" form for a model
 -->
<div class="modal fade model new" id='new-form-modal'>
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Add <span class='model-type'></span></h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning edit cancel">Cancel</button>
        <button type="button" class="btn btn-success edit submit-form">Submit</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



