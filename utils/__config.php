<?php
	/**
	* This file has all includes that are needed in the site as well as some useful global variables
	*/

	//The whole site should be in sessions
	session_start();
	session_regenerate_id();

	//Include this file in every php file that needs to access utils
	//Will provide the $abroot for finding other files easily
	//Place the debug flag here if degubbing code should run
	$debug = true; //Must be set to true for debug code to run
	$force_errors = true; //If we want to force the server to show all php errors

	if($debug && $force_errors){
		error_reporting(E_ALL);
		ini_set('display_errors', '1');
	}
	else{
		error_reporting(0);
		ini_set('display_errors', '0');
	}

	/************************************************
				Folder and Web Path
	************************************************/
	$root_folder_name = 'inventory';
	$abroot_start = strpos(getcwd(), $root_folder_name);
	$abroot = substr(getcwd(),0,$abroot_start + strlen($root_folder_name)); //So this string should be something like /var/www/inventory, we can use it as include "$abroot/utils/db/database.php"
	$webroot = '/inventory'; //For use in html and javascript, should be used like href="<?php echo $webroot ? >/something"


	/************************************************
					Error Reporting
	************************************************/
	if($force_errors){
		error_reporting(E_ALL);
		ini_set('display_errors', '1');
	}

	/************************************************
					PHPASS Configurations
	************************************************/
	$phpass_log2 = 1024;
	$phpass_portable = false; //Should never be true, unless on old version of PHP with no bycrypt


	/************************************************
					REGEX's
	************************************************/
	$regex_username = "/^[a-z0-9\-]+$/";
	$regex_user_id = "/^[0-9]+$/";


	/************************************************
					Includes (that are not classes)
	************************************************/
	require_once $abroot . '/utils/__exceptions.php';
	require_once $abroot . '/utils/db/database.php';
	require_once $abroot . '/utils/users/PasswordHash.php';
	require_once $abroot . '/utils/users/password.php';
	require_once $abroot . '/utils/users/user_functions.php';


	/************************************************
					Classes
	************************************************/
	require_once $abroot . '/classes/Model.php';
	require_once $abroot . '/classes/Person.php';
	require_once $abroot . '/classes/Location.php';
	require_once $abroot . '/classes/assets/Asset.php';
	require_once $abroot . '/classes/assets/Computer.php';
	require_once $abroot . '/classes/assets/Software.php';
	require_once $abroot . '/classes/loans/LoanController.php';

	require_once $abroot . '/classes/mappers/I_DataMapper.php';
	require_once $abroot . '/classes/mappers/ComputerMapper.php';
	require_once $abroot . '/classes/mappers/SoftwareMapper.php';
	require_once $abroot . '/classes/mappers/PersonMapper.php';
	require_once $abroot . '/classes/mappers/LocationMapper.php';

	require_once $abroot . '/classes/factory/I_ModelFactory.php';
	require_once $abroot . '/classes/factory/ModelFactory.php';

	/************************************************
					Other
	************************************************/
	$general_error_message = "We're sorry, something went wrong. If this continues, please contact your administrator.";//Generic error message to be displayed
?>