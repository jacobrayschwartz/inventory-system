<?php
	/**
	* Connecting to a database
	* Throws exception if connection doesn't work
	*/

	/**
	* Returns a mysqli object using the connection info in db_con_info.php
	* @return mysql
	*/
	function getDBConn(){
		require $GLOBALS['abroot'] . "/utils/db/db_conn_info.php";

		$db = new mysqli($dddr, $dser, $dass, $dame);
		if($db->connect_errno){
			throw new DBException('Error connecting to database!' . (isset($GLOBALS['debug']) && $GLOBALS['debug'] === true ? '<br/><strong>' . $db->connect_error . '</strong>' : ''));
		}
		return $db;
	}
?>