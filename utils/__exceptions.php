<?php
	/**
	* This file contains all of the custom exceptions for this project that are not contained in other classes
	*/
	 
	

	/**
	* A custom exception for database related issues. 
	* This exception is mostly for readability, it doesn't do anything extra
	* The database exception message may contain secure information. IT SHOULD NEVER BE DISPLAYED UNLESS YOU ARE DEBUGGING!
	*/
	class DBException extends Exception{
		public function __construct($message, $code = 0, Exception $previous = null){
			parent::__construct($message, $code, $previous);
		}

		public function __toString(){
			return 'DATABASE ERROR - ' . $this->message;
		}
	}


	/**
	* A custom exception for validation issues.
	* This message is made for production and debugging, it contains 2 message values
	*/
	class ValidationException extends Exception{
		private $user_message = '';//message to be displayed to the user

		/**
		* @param string $user_message Message to display normally
		* @param string $internal_message Message to log and display during debugging, passed to the Exception class
		*/
		public function __construct($user_message, $internal_message, $code = 0, Exception $previous = null){
			$this->user_message = $user_message;
			parent::__construct($internal_message, $code, $previous);
		}

		public function __toString(){
			return 'VALIDATION ERROR - ' . $this->message;
		}

		public function getUserMessage(){
			return $this->user_message;
		}
	}

	/**
	* A custom exception for missing models
	* Contains two messages, user and internal
	* Should be thrown when a model is REQUIRED
	*/
	class RequiredModelException extends Exception{
		private $user_message = '';

		/**
		* @param string $user_message Message to display normally
		* @param string $internal_message Message to log and display during debugging, passed to the Exception class
		*/
		public function __construct($model_type, $internal_message, $code = 0, Exception $previous = null){
			$this->user_message = "We're sorry, that $model_type is not here. Please add it before referencing.";
			parent::__construct($internal_message, $code, $previous);
		}

		public function __toString(){
			return 'REQUIRED MODEL MISSING - ' . $this->message;
		}

		public function getUserMessage(){
			return $this->user_message;
		}
	}

	/**
	* A custom exception for Loans
	* Contains two messages, user and internal
	* Should be thrown when an asset is already checked out
	*/
	class LoanException extends Exception{
		private $user_message = '';

		/**
		* @param string $user_message Message to display normally
		* @param string $internal_message Message to log and display during debugging, passed to the Exception class
		*/
		public function __construct($user_message, $internal_message, $code = 0, Exception $previous = null){
			$this->user_message = $user_message;
			parent::__construct($internal_message, $code, $previous);
		}

		public function __toString(){
			return 'LOAN EXCEPTION - ' . $this->message;
		}

		public function getUserMessage(){
			return $this->user_message;
		}
	}
?>