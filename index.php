<?php
	require_once './utils/__config.php';

	//If the user isn't valid, boot them to the login page
	if(!validateCurrentUser()){
		header('Location: /inventory/login');
		die(' ');
	}


	$software = new Software('Microsoft Office Professional', 
							null, 
							null, 
							null, 
							'SOFTWARE_MS_1', 
							'1213-8787-6664-1234-4887-9888', 
							'2013', 
							null, 
							'Some note',
							null,
							true,
							'1990-05-30');
	$db = getDBConn();
	$mapper = SoftwareMapper::GetInstance($db);
	$mapper->create($software);

	//Keeping the modal html in a separate file for clarity's sake
	include "$abroot/utils/html/modals.php";

	//$db = getDBConn();
	//$loan_controller = LoanController::GetInstance($db);

	//var_dump($loan_controller->getAllAssetsCheckedOut());
	//$computerMapper = ComputerMapper::GetInstance($db);
	//$computer = $computerMapper->find(['computer544a867b56f2e0.60511953'])[0];
	//var_dump($computer->getLoanStatus());

	//$loan_controller->checkIn('1', '2014-11-10 12:30:00', "Note");
?>


<!DOCTYPE html>
<html>
<head>
	<title>Inventory</title>
	<link rel="stylesheet" type="text/css" href="<?php echo $webroot ?>/styles/main.css">
</head>
<body>
	<nav class='navbar-default navbar' role='navigation'>
		<div class='container-fluid'>
			<div class='navbar-header'>
				<div class='navbar-brand'>Inventory</div>
			</div><!-- end .navbar-header -->
			<div class='navbar-form navbar-left'>
				<select id='view-selection' name='view-selection' class='form-control'>
					<option selected value='computers'>Computers</option>
					<option value='locations'>Locations</option>
					<option value='people'>People</option>
					<option value='software'>Software</option>
				</select>
			</div>

			<div class='navbar-right'>
				<div class='username navbar-brand'>Hello <?php echo $_SESSION['user_username'] ?>!</div>
				<ul class='nav navbar-nav'>
					<li><a href="<?php echo $webroot ?>/login">Logout</a></li>
				</ul>
			</div><!-- end .navbar-right -->
		</div> <!-- end .container-fluid -->
	</nav> <!-- end nav -->

	<aside class='content'>
		<div class='col-lg-2 col-md-3 side-panel'>
			<ul>
				<li><a>View All</a></li>
			</ul>
			<div class='form-group'>
				<div class='input-group'>
					<div class='input-group-addon'><div class='fa fa-search'></div></div>
					<input type='text' class='form-control autocomplete' id='listing-search'>
					<div class='input-group-btn'><button class=' btn btn-primary'>Go</button></div>
				</div>
			</div>
			<button class='btn btn-success' id='addItem' style='width: 100%;'>Add</button>
		</div>

		<div class='col-md-9 col-lg-10' id='content-pane'>
			<div style='margin-top: 25px;'></div>  <!-- Spacer -->
			<div class='panel panel-default' id='listing-panel'>
				<div class='panel-heading'></div>
				<table class='table table-striped table-hover'>
					<thead></thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</aside><!-- end .content -->
	

	

	<script src='https://code.jquery.com/jquery-2.1.1.min.js'></script>
	<!-- Scripts from Bower-->
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/affix.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/alert.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/button.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/carousel.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/dropdown.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/modal.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/popover.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/scrollspy.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/tab.js'></script>
	<script src='<?php echo $webroot ?>/bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js'></script>



	<!-- Scripts -->
	<script type="text/javascript">
		//Global Page Variables
		var webroot = '/inventory'; //For finding scripts from the javascript side
		var curList; //The current list (pointer)
		var allComputers; //Listing of copmuters from the database
		var filteredComputers; //Listing of computers based on the search
		var allLocations; //Listing of all locations
		var filteredLocations; //Listing of locations based on search
		var allPeople; //Listing of all people from database
		var filteredPeople; //Listing of people based on a search
		var allSoftware;
		var filteredSoftware;
		var loanList;
	</script>

	<script src='<?php echo $webroot ?>/js/data/computers.js'></script>
	<script src='<?php echo $webroot ?>/js/data/locations.js'></script>
	<script src='<?php echo $webroot ?>/js/data/people.js'></script>
	<script src='<?php echo $webroot ?>/js/data/software.js'></script>
	<script src='<?php echo $webroot ?>/js/data/loans.js'></script>
	<script src='<?php echo $webroot ?>/js/modals.js'></script>
	<script src='<?php echo $webroot ?>/js/listing.js'></script>
	<script src='<?php echo $webroot ?>/js/alerts.js'></script>
  	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

	<script type="text/javascript">
		$( document ).ready(function(){
			updateData();
		});

		$('#addItem').on('click', function(e){
			var model = $('#view-selection option:selected').attr('value');
			var url = '';
			var title = '';
			switch(model){
				case 'computers':
					url = '<?php echo $webroot ?>/utils/data/computers/getNewComputerForm.php';
					title = 'Computer';
					break;
				case 'locations':
					url = '<?php echo $webroot ?>/utils/data/locations/getNewLocationForm.php';
					title = 'Location';
					break;
				case 'people':
					url = '<?php echo $webroot ?>/utils/data/people/getNewPersonForm.php';
					title = 'Person';
					break;
				case 'software':
					url = '<?php echo $webroot ?>/utils/data/software/getNewSoftwareForm.php';
					title = 'Software';
					break;
				default:
					url = '<?php echo $webroot ?>/utils/data/computers/getNewComputerForm.php';
					title = 'Computer';
			}
			$.post(url,
				function(response){
					response = JSON.parse(response);
					if(!response.error){
						$('#new-form-modal .modal-body').html(response.data);

						//Hiding the edit fields
						$('#new-form-modal .value').remove();
						$('#new-form-modal .model-type').text(title);
						$('#new-form-modal .edit').show();
						$('#new-form-modal').modal();

					}
					else{//If there was an issue
						var a = generateAlert(response.data, 'danger');
						a.hide();
						$('#content-pane').prepend(a);
						a.slideDown();
					}
				});
		});


		//$('.autocomplete').live('keydown',function(){
			$('.autocomplete').autocomplete({
					source: function(request, response){
						var list = allPeople;
						var query = request.term;
						var matches = [];

						for(var i = 0; i < list.length; i++){
							alert();
							if(list[i].first == query || list[i].last == query){
								matches.push(list[i]);

							}
						}

						response(matches);
					},
					focus: function(e, ui){
						e.preventDefault();
					},
					select: function(event, ui){
						e.preventDefault();
						$(".autocomplete").val(ui.item.employee_id);
					}
			});
		//});

		
	</script>
</body>
</html>