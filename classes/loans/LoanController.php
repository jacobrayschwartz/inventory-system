<?php
	class LoanController{
		
		private static $instance = null;
		private $db;

		public static $regex_loan_id = "/^[0-9]+$/";

		private function __construct($db){
			$this->db = $db;
		}

		/**
		* Once nothing references this singleton, close the db connection
		*/
		/*public function __destruct(){
			if(isset($this->db) && $this->db !== null){
				$this->db->close();
			}
			unset($this->db);
		}*/


		public static function GetInstance($db){
			if(LoanController::$instance === null){
				if($db == null){
					throw new DBException($GLOBALS['debug'] ? "Database can't be null!" : $GLOBALS['general_error_message']);
				}
				LoanController::$instance = new LoanController($db);
			}
			return LoanController::$instance;
		}

		/**
		* Making sure this object can't be cloned
		*/
		private function __clone(){
			//Making sure the object cant be cloned
		}

		/**
		* Making sure this object can't be unserialized
		*/
		private function __wakeup(){
			//Making sure the object can't be unserialized
		}



		public function getStatus($asset){
			$asset_id = '';
			$status = '';
			//Because we can't do normal function overloading in PHP, I have to check what type of variable this is
			if(is_subclass_of($asset, 'Asset')){
				$asset_id = $asset->getDBID();
			}
			else if(gettype($asset) == 'string'){
				$asset_id = $asset;
			}
			else{
				throw new ValidationException("Sorry, that device does not exist.", "VALIDATION EXCEPTION - NOT STRING OR ASSET");
			}

			//Checking for a vaild id string
			if(!preg_match(Model::$regex_dbid, $asset_id)){
				throw new ValidationException("Sorry, that device does not exist.", "VALIDATION EXCEPTION - BAD ID STRING FORMAT");
			}

			//Checking to see if the asset exists
			$query = "SELECT COUNT(id) FROM assets WHERE id=?";
			$stmt = $this->db->prepare($query);
			$stmt->bind_param('s',$asset_id);
			$stmt->execute();

			$count = null;
			$stmt->bind_result($count);
			$stmt->fetch();
			$stmt->free_result();
			unset($stmt);

			if($count != 1){
				throw new RequiredModelException("Asset", "Asset does not exist!");
			}

			//If we're here, we can check the database for current loans based on this id
			$query = "SELECT id, (CASE WHEN end_date < CURRENT_DATE() THEN 'overdue' ELSE 'checked-out' END) as status FROM loans WHERE asset_id=? AND turned_in IS null";
			$stmt = $this->db->prepare($query);
			$stmt->bind_param('s',$asset_id);
			$stmt->execute();
			$stmt->store_result();

			//If there were no rows returned, then the asset is not checked out
			if($stmt->num_rows === 0){
				$status = "Checked In";
			}
			else{
				$loan_id;
				$stmt->bind_result($id, $status);
				$stmt->fetch();
				$stmt->free_result();
				$status;
			}

			return $status;
		}

		/**
		* Returns the asset id and status of every asset that is currently out
		*/
		public function getAllAssetsCheckedOut(){
			$assets = array();
			$query = "SELECT id, asset_id, (CASE WHEN end_date < CURRENT_DATE() THEN 'overdue' ELSE 'checked-out' END) as status FROM loans WHERE turned_in IS NULL AND start_date < CURDATE()"; 
			$rs = $this->db->query($query);
			while($row = $rs->fetch_assoc()){
				$assets[] = $row;
			}
			

			return $assets;
		}

		public function getLoanHistory($asset){
			$asset_id = '';
			//Because we want to add the person object to the array
			$personMapper = PersonMapper::GetInstance($this->db);

			//Because we can't do normal function overloading in PHP, I have to check what type of variable this is
			if(is_subclass_of($asset, 'Asset')){
				$asset_id = $asset->getDBID();
			}
			else if(gettype($asset) == 'string'){
				$asset_id = $asset;
			}
			else{
				throw new ValidationException("Sorry, that device does not exist.", "VALIDATION EXCEPTION - NOT STRING OR ASSET");
			}

			//Checking for a vaild id string
			if(!preg_match(Model::$regex_dbid, $asset_id)){
				throw new ValidationException("Sorry, that device does not exist.", "VALIDATION EXCEPTION - BAD ID STRING FORMAT");
			}

			//Checking to see if the asset exists
			$query = "SELECT COUNT(id) FROM assets WHERE id=?";
			$stmt = $this->db->prepare($query);
			$stmt->bind_param('s',$asset_id);
			$stmt->execute();
			$loans = array();

			$count = null;
			$stmt->bind_result($count);
			$stmt->fetch();
			$stmt->free_result();
			unset($stmt);

			if($count != 1){
				throw new RequiredModelException("Asset", "Asset does not exist!");
			}


			$loan_id;
			$start_date;
			$end_date;
			$turned_in;
			$person_id;
			$notes;
			$person_id_list = array();
			$people;

			$query = "SELECT id, start_date, end_date, turned_in, person_id, notes FROM loans WHERE asset_id=? ORDER BY start_date";
			$stmt = $this->db->prepare($query);
			$stmt->bind_param('s',$asset_id);
			$stmt->execute();
			$stmt->bind_result($loan_id, $start_date, $end_date, $turned_in, $person_id, $notes);

			while($stmt->fetch()){
				$loans[] = array('id' => $loan_id, 'start_date' => $start_date, 'end_date' => $end_date, 'turned_in' => $turned_in, 'person_id' => $person_id, 'notes' => $notes);
				$person_id_list[] = $person_id;
			}

			$people = $personMapper->find($person_id_list);

			for($i = 0; $i < count($people); $i++){
				$id = $people[$i]->getDBID();

				//The find function should return the people in the same order
				if($id == $loans[$i]){
					$loans[$i]['person'] = $people[$i];
				}
				//But just in case
				else{
					for($j = 0; $j < count($loans); $j++){
						if($id == $loans[$j]){
							$loans[$j]['person'] = $people[$j];
						}
					}

				}
			}


			//Finally, we can return that list
			return $loans;
		}

		/**
		*	This function will check a loaned item back in
		*/
		public function checkIn($loan_id, $time, $notes = null){
			//First validating the loan id
			if(!preg_match(LoanController::$regex_loan_id, $loan_id)){
				throw new ValidationException("Sorry, that loan does not exist", "VALIDATION ERROR - BAD LOAN ID FORMAT");
			}
			//Validating the time, must be a valid string
			if(DateTime::createFromFormat('Y-m-d G:i:s', $time) === false){
				throw new ValidationException("Sorry, that date is not valid", "VALIDATION ERROR - BAD DATE FORMAT");
			}
			//Looking for the loan in the database
			$stmt = $this->db->prepare("SELECT * FROM loans WHERE id=?");
			$stmt->bind_param("i",$loan_id);
			$stmt->execute();
			$stmt->store_result();
			if($stmt->num_rows !== 1){
				throw new RequiredModelException("Loan", "That loan does not exist!");
			}

			//If the loan exists, we can check it in
			$query = "UPDATE loans SET turned_in=?, notes=(CONCAT(notes, ?)) WHERE id=?";
			$stmt = $this->db->prepare($query);
			$stmt->bind_param('ssi',$time, $notes, $loan_id);
			$stmt->execute();

		}


		public function checkOut($asset_id, $start_date, $end_date, $notes = null){
			if(!preg_match(Model::$regex_dbid, $asset_id)){
				throw new ValidationException("Sorry, that asset does not exist", "VALIDATION ERROR - BAD MODEL ID FORMAT");
			}


			try{
				$start_date_obj = new DateTime($start_date);
				$end_date_obj   = new DateTime($end_date);
			}
			//Casting the general exception to a validation exception
			catch(Exception $e){
				throw new ValidationException("Sorry, that is an invalid date", "VALIDATION ERROR - BAD DATE FORMAT - " . $e->getMessage());
			}

			if($end_date_obj < $start_date_obj){
				throw new ValidationException("The end date cannot be before the start date", "VALIDATION ERROR - BAD DATE RANGE");
			}

			//Checking to see if the asset exists
			$query = "SELECT id FROM assets WHERE id=?";
			$stmt = $this->db->prepare($query);
			$stmt->bind_param('s',$asset_id);
			$stmt->execute();
			$stmt->store_result();
			$num_rows = $stmt->num_rows;
			$stmt->free_result();
			//If the asset does not exist,
			if($num_rows != 1){
				throw new RequiredModelException("Asset", "Asset does not exist!");
			}

			//Checking out the asset
			$query = "INSERT INTO loans SET start_date = ?, end_date = ?, asset_id = ?";
			$stmt = $this->db->prepare($query);
			$stmt->bind_param('sss', $start_date_obj->format('Y-m-d'), $end_date_obj->format('Y-m-d'), $asset_id);
			if(!$stmt->execute()){
				$error = $stmt->error;
				$error_num = $stmt->$errorno;


				if($error_num == 45000){
					throw new ValidationException($error, $error);
				}
				else{
					throw new DBException($GLOBALS['debug'] ? "Database error! $error" : $GLOBALS['general_error_message']);
				}
			}

		}
	}
?>