<?php 
	/**
	* A class file for Computer
	*/
	

	/**
	* The Computer Class
	* Extends Asset
	*/

	class Computer extends Asset{
		/**
		* Disk space on the computer
		* @var float
		*/
		private $disk_space;

		/**
		* RAM on the computer
		* @var float
		*/
		private $memory;

		/**
		*Processor on the computer
		* @var string
		*/
		private $processor;

		/**
		* Operating on the computer
		* @var string
		*/
		private $os;

		/**
		* Type of computer
		* @var string
		*/
		private $type;

		
		/**
		* Constructor for the Computer asset
		* 
		* @param string $name
		* @param int $parent_id
		* @param int $person_id
		* @param int $location_id
		* @param string $asset_tag
		* @param float $disk_space
		* @param float $memory
		* @param string $processor
		* @param string $os
		* @param string $type
		* @param string $notes
		* @param int $dbid If this value is null (for creating), it will be generated
		* @param boolean $archived
		* @param Date archivedOn
		* @throws ValidationException
		*/
		public function __construct($name = '', 
									$parent_id = null, 
									$person_id = null, 
									$location_id = null, 
									$asset_tag = null, 
									$disk_space = '', 
									$memory = '', 
									$processor = '', 
									$os = '', 
									$type = '',
									$notes = null,
									$dbid = null,
									$archived = false,
									$archived_on = null){
			/*
			Error checking
			Want to return all errors in the message, so using a boolean value to mark if there is an issue
			However, we can only show the exception for either the parent or the child - possible work around 
			*/
			$is_valid = true;
			$user_message = '';
			$internal_message = '';

			//dbid
			if($dbid === null){
				$dbid = Model::createModelID('computer');
			}

			//disk_space
			if(!preg_match(Model::$regex_pos_number, $disk_space)){
				$is_valid = false;
				$user_message .= "The disk space must be a positive number<br/>";
				$internal_message .= "Invalid disk_space<br/>";
			}

			//memory
			if(!preg_match(Model::$regex_pos_number, $memory)){
				$is_valid = false;
				$user_message .= "The memory must be a positive number<br/>";
				$internal_message .= "Invalid memory<br/>";
			}

			//processor
			if(!preg_match(Model::$regex_string_short, $processor)){
				$is_valid = false;
				$user_message .= "The processor can only contain letters, numbers, spaces, dashes and periods<br/>";
				$internal_message .= "Invalid processor<br/>";
			}

			//os
			if(!preg_match(Model::$regex_string_short, $os)){
				$is_valid = false;
				$user_message .= "The Operating System can only contain letters, numbers, spaces, dashes and periods<br/>";
				$internal_message .= "Invalid os<br/>";
			}

			//type
			if(!preg_match(Model::$regex_string_short, $type)){
				$is_valid = false;
				$user_message .= "The type can only contain letters, numbers, spaces, dashes and periods<br/>";
				$internal_message .= "Invalid type<br/>";
			}

			if(!$is_valid){
				throw new ValidationException($user_message, "VALIDATION EXCEPTION - Computer Constructor - $internal_message");
			}

			$this->disk_space = $disk_space;
			$this->memory = $memory;
			$this->processor = $processor;
			$this->os = $os;
			$this->type = $type;


			parent::__construct($dbid, $name, $parent_id, $person_id, $location_id, $asset_tag, $notes, $archived, $archived_on);
		}

		/**
		* @see Model::getInfoArray() \inventory\Asset
		*/
		public function getInfoArray(){
			$array = parent::getInfoArray(); //Getting the parent's info
			$array['disk_space'] = $this->disk_space;
			$array['memory'] = $this->memory;
			$array['processor'] = $this->processor;
			$array['os'] = $this->os;
			$array['type'] = $this->type;

			return $array;
		}

		/**
		* @see Model::getInfo() \inventory\Asset
		*/
		public function getInfo(){
			return json_encode($this->getInfoArray());
		}


		public function getHTML(){
			$name = strtoupper($this->name);
			$person = $this->getPerson(); //Getting the person object for its data
			$location = $this->getLocation(); //Getting the location object

			return "<form action='" . $GLOBALS['webroot'] . "/utils/data/computers/addUpdateComputer.php'>
				      <input type='hidden' name='id' value='$this->dbid'/>
				      

				      <div class='row'>
				            <!-- Name of the computer -->
				            <div class='name col-md-7'>
				                  <h2 class='value'><div class='fa fa-laptop'></div> $name</h2>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-laptop'></div></div>
				                              <input type='text' class='form-control' name='name' placeholder='Name of computer' value='$this->name'/>
				                        </div>
				                  </div>
				            </div><!-- End .name -->

				            
				      </div><!-- End .row -->
				      
				      <div class='row'>
				            <!-- Asset Tag -->
				            <div class='asset-tag col-md-4'>
				                  <div class='value'><div class='fa fa-tag'></div> <b>$this->asset_tag</b></div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-tag'></div></div>
				                              <input type='text' class='form-control' name='asset_tag' placeholder='Asset Tag' value='$this->asset_tag'/>
				                        </div>
				                  </div>
				            </div><!-- End .asset-tag -->

				            <!-- User -->
				            <div class='user col-md-4'>
				                  <div class='value'><div class='fa fa-user'></div>" . (($person != null) ?  " <a class='record' data-dbid='$this->person_id'><b> " . $person->getLast() . ", " . $person->getFirst() . " </b></a>": ' None') . "</div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-user'></div></div>
				                              <input type='text' class='form-control autocomplete' name='employee_id' data-autotarget='people' placeholder='Employee ID' value='" . (($person != null) ? $person->getEmployeeID() : '') . "'/>
				                        </div>
				                  </div>
				            </div><!-- End .user -->

				      <!-- Location -->
				            <div class='location col-md-4'>
				                  <div class='value'><div class='fa fa-building'></div> <b>" . (($location != null) ? " <a class='record' data-dbid='$this->location_id'><b> " . $location->getBuilding() . " " . $location->getNumber() . " </b></a>" : ' None' ) . "</b></div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-building'></div></div>
				                              <input type='text' class='form-control' name='location' placeholder='Location (SNR 338)' value='" . (($location != null) ? $location->getBuilding() . " " . $location->getNumber() : '') . "'/>
				                        </div>
				                  </div>
				            </div><!-- End .location -->
				      </div><!-- End .row -->

				      <div class='h-divider-dark value'></div>

				      <div class='row'>
				      <!-- Disk Space -->
				            <div class='disk-space col-md-4'>
				                  <div class='value'>HDD: <b>$this->disk_space GB</b></div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-hdd-o'></div></div>
				                              <input type='text' class='form-control' name='disk_space' placeholder='Disk Space (GB)' value='$this->disk_space'/>
				                        </div>
				                  </div>
				            </div><!-- End .disk-space -->

				      <!-- Memory -->
				            <div class='memory col-md-4'>
				                  <div class='value'>RAM: <b>$this->memory GB</b></div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>RAM</div>
				                              <input type='text' class='form-control' name='memory' placeholder='Memory (GB)' value='$this->memory'/>
				                        </div>
				                  </div>
				            </div><!-- End .memory -->

				            <!-- Operating System -->
				            <div class='os col-md-4'>
				                  <div class='value'>OS: <b>$this->os</b></div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>OS</div>
				                              <input type='text' class='form-control' name='os' placeholder='Operating System' value='$this->os'/>
				                        </div>
				                  </div>
				            </div><!-- End .os -->
				      </div><!-- End .row -->
				      
				      <div class='row'>
				            <!-- Processor -->
				            <div class='processor col-md-6'>
				                  <div class='value'>Processor: <b>$this->processor</b></div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>Proc.</div>
				                              <input type='text' class='form-control' name='processor' placeholder='Processor' value='$this->processor'/>
				                        </div>
				                  </div>
				            </div><!-- End .type -->

				            <!-- Type -->
				            <div class='type col-md-6'>
				                  <div class='value'>Type: <b>" . ucfirst($this->type) . "</b></div>
				                  <div class='form-group edit'>
				                        <select class='form-control' name='type'>
				                              <option " . ((strtolower($this->type) == 'desktop') ? 'selected' : '') . " value='desktop'>Desktop</option>
				                              <option " . ((strtolower($this->type)  == 'laptop') ? 'selected' : '') . " value='laptop'>Laptop</option>
				                              <option " . ((strtolower($this->type)  == 'server') ? 'selected' : '') . " value='server'>Server</option>
				                        </select>
				                  </div>
				            </div><!-- End .type -->
				      </div><!-- End .row -->

				      <div class='h-divider-dark value'></div>

				      <div class='row'>
				            <!-- Notes -->
				            <div class='col-md-12 notes'>
				                  <div class='value'><b>Notes: </b><br>$this->notes</div>
				                  <textarea name='notes' class='edit form-control'>$this->notes</textarea>
				            </div><!-- end .notes -->
				            
				      </div><!-- End .row -->

				</form>";
		}

		
		/***************************
			Getters and Setters
		***************************/

		/**
		* Getter for the Computer Disk Space
		* 
		* @return float
		*/
		public function getDiskSpace(){
			return $this->disk_space;
		}

		/**
		* Setter for the Computer Disk Space
		* 
		* @param float $disk_space
		* @throws ValidationException
		*/
		public function setDiskSpace($disk_space){
			if(!preg_match(Model::$regex_pos_number, $disk_space)){
				throw new ValidationException("The disk space must be a positive number<br/>", "VALIDATION ERROR - DISK_SPACE: $disk_space");
			}

			$this->disk_space = $disk_space;
		}

		/**
		* Getter for the Computer's Memory
		* 
		* @return float
		*/
		public function getMemory(){
			return $this->memory;
		}

		/**
		* Setter for the Computer's Memory
		* 
		* @param float $memory
		* @throws ValidationException
		*/
		public function setMemory($memory){
			if(!preg_match(Model::$regex_pos_number, $memory)){
				throw new ValidationException("The memory can only contain letters, numbers, spaces, dashes and periods<br/>", "VALIDATION ERROR - MEMORY: $memory");
			}

			$this->memory = $memory;
		}

		/**
		* Getter for the Computer's processor
		* 
		* @return string
		*/
		public function getProcessor(){
			return $this->processor;
		}

		/**
		* Setter for the Computer's processor
		* 
		* @param string $processor
		* @throws ValidationException
		*/
		public function setProcessor($processor){
			if(!preg_match(Model::$regex_string_short, $processor)){
				throw new ValidationException("The processor can only contain letters, numbers, spaces, dashes and periods<br/>", "VALIDATION ERROR - PROCESSOR: $processor");
			}

			$this->processor = $processor;
		}

		/**
		* Getter for the Computer's Operating System
		* 
		* @return string
		*/
		public function getOS(){
			return $this->os;
		}

		/**
		* Setter for the Computer's Operating System
		* 
		* @param string $os
		* @throws ValidationException
		*/
		public function setOS($os){
			if(!preg_match(Model::$regex_string_short, $os)){
				throw new ValidationException("The Operating System can only contain letters, numbers, spaces, dashes and periods<br/>", "VALIDATION ERROR - OS: $os");
			}

			$this->os = $os;
		}

		/**
		* Getter for the Computer's Type (Laptop, Desktop, etc)
		* 
		* @return string
		*/
		public function getType(){
			return $this->type;
		}

		/**
		* Setter for the Computer's Type (Laptop, Desktop, etc)
		* 
		* @param string $type
		* @throws ValidationException
		*/
		public function setType($type){
			if(!preg_match(Model::$regex_string_short, $type)){
				throw new ValidationException("The type can only contain letters, numbers, spaces, dashes and periods<br/>", "VALIDATION ERROR - Type: $type");
			}

			$this->type = $type;
		}



		/**
		* This function returns a blank edit form (for creating new computers)
		* @return string
		*/
		public static function GetNewHTML(){

			return "<form action='" . $GLOBALS['webroot'] . "/utils/data/computers/addUpdateComputer.php'>
				      <input type='hidden' name='id' value=''/>
				      

				      <div class='row'>
				            <!-- Name of the computer -->
				            <div class='name col-md-7'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-laptop'></div></div>
				                              <input type='text' class='form-control' name='name' placeholder='Name of computer' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .name -->

				            
				      </div><!-- End .row -->
				      
				      <div class='row'>
				            <!-- Asset Tag -->
				            <div class='asset-tag col-md-4'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-tag'></div></div>
				                              <input type='text' class='form-control' name='asset_tag' placeholder='Asset Tag' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .asset-tag -->

				            <!-- User -->
				            <div class='user col-md-4'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-user'></div></div>
				                              <input type='text' class='form-control' name='employee_id' placeholder='Employee ID' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .user -->

				      <!-- Location -->
				            <div class='location col-md-4'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-building'></div></div>
				                              <input type='text' class='form-control' name='location' placeholder='Location (SNR 338)' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .location -->
				      </div><!-- End .row -->

				      <div class='h-divider-dark value'></div>

				      <div class='row'>
				      <!-- Disk Space -->
				            <div class='disk-space col-md-4'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-hdd-o'></div></div>
				                              <input type='text' class='form-control' name='disk_space' placeholder='Disk Space (GB)' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .disk-space -->

				      <!-- Memory -->
				            <div class='memory col-md-4'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>RAM</div>
				                              <input type='text' class='form-control' name='memory' placeholder='Memory (GB)' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .memory -->

				            <!-- Operating System -->
				            <div class='os col-md-4'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>OS</div>
				                              <input type='text' class='form-control' name='os' placeholder='Operating System' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .os -->
				      </div><!-- End .row -->
				      
				      <div class='row'>
				            <!-- Processor -->
				            <div class='processor col-md-6'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>Proc.</div>
				                              <input type='text' class='form-control' name='processor' placeholder='Processor' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .type -->

				            <!-- Type -->
				            <div class='type col-md-6'>
				                  <div class='form-group edit'>
				                        <select class='form-control' name='type'>
				                              <option selected value='desktop'>Desktop</option>
				                              <option value='laptop'>Laptop</option>
				                              <option value='server'>Server</option>
				                        </select>
				                  </div>
				            </div><!-- End .type -->
				      </div><!-- End .row -->

				      <div class='h-divider-dark value'></div>

				      <div class='row'>
				            <!-- Notes -->
				            <div class='col-md-12 notes'>
				                  <textarea name='notes' class='edit form-control'></textarea>
				            </div><!-- end .notes -->
				            
				      </div><!-- End .row -->

				</form>";
		}
	}


	

?>