<?php 
	/**
	* A class file for Asset
	*/
	
	/**
	* The Asset Class
	* An abstract class for the various types of assets we have (computers, monitors, cameras, etc)
	* Uses mapper classes (datamapper pattern) for CRUD in the database, each concrete class will have its own mapper singleton
	* 
	* 
	*/

	abstract class Asset extends Model{

		/**
		* Name for this asset
		* @var string
		* 
		*/
		protected $name;

		/**
		* DBID for this asset's parent
		* @var int
		* 
		*/
		protected $parent_id;

		/**
		* DBID for this asset's user
		* @var int
		* 
		*/
		protected $person_id;

		/**
		* DBIDfor this asset's location
		* @var int
		* 
		*/
		protected $location_id;

		/**
		* Asset Tag
		* @var string
		* 
		*/
		protected $asset_tag; //Company asset tag

		/**
		* Notes for the asset
		* @var string
		* 
		*/
		protected $notes; //Company asset tag

		/**
		* If this asset has been archived (surplussed, moved, etc)
		* @var boolean
		*/

		/**
		* When this asset was archived (nullable)
		* @var Date
		*/

		/**
		* Constructor for the Asset class
		* 
		* @param int $dbid
		* @param string $name
		* @param int $parent_id
		* @param int $person_id
		* @param int $location_id
		* @param string $asset_tag
		* @param string $notes
		* @param boolean $archived
		* @param Date archivedOn
		* @throws ValidationException
		*/
		protected function __construct($dbid = '', $name = '', $parent_id = null, $person_id = null, $location_id = null, $asset_tag = null, $notes = null, $archived = false, $archived_on = null){
			/*
			Error checking
			Want to return all errors in the message, so using a boolean value to mark if there is an issue
			*/
			$is_valid = true;
			$user_message = '';
			$internal_message = '';
			
			

			//name
			if(!preg_match(Model::$regex_string_short, $name)){
				$is_valid = false;
				$user_message .= "The name can only contain letters, numbers, spaces, dashes and periods<br/>";
				$internal_message .= "Invalid name<br/>";
			}

			//parent_id
			if($parent_id !== null && !preg_match(Model::$regex_dbid, $parent_id)){
				$is_valid = false;
				$user_message .= "That parent asset is not valid<br/>";
				$internal_message .= "Invalid parent id<br/>";
			}

			//person_id
			if($person_id !== null && !preg_match(Model::$regex_dbid, $person_id)){
				$is_valid = false;
				$user_message .= "That user is not valid<br/>";
				$internal_message .= "Invalid user dbid<br/>";
			}

			//location_id
			if($location_id !== null && !preg_match(Model::$regex_dbid, $location_id)){
				$is_valid = false;
				$user_message .= "That location is not valid<br/>";
				$internal_message .= "Invalid location dbid<br/>";
			}

			//asset_tag
			if($asset_tag !== null && !preg_match(Model::$regex_asset_tag, $asset_tag)){
				$is_valid = false;
				$user_message .= "The asset tag can only contain letters, numbers, spaces, dashes and periods<br/>";
				$internal_message .= "Invalid asset tag<br/>";
			}

			//archived date
			if(!$archived){
				//If the archived flag is not set, the date should be null
				$archived_on = null;
			}
			if($archived && $archived_on == null){
				$is_valid = false;
				$user_message .= "There was an issue, the achived date was not set, but the asset was marked as archived<br/>";
				$internal_message .= "Archived marked, but archived_on is null!<br/>";
			}
			if($archived){
				try{
					$archived_on = new DateTime($archived_on);
				}
				catch(Exception $e){
					$is_valid = false;
					$user_message .= "The archived date is not valid.<br/>";
					$internal_message .= "Bad archived_on date! - $e<br/>";
				}
				if(!$archived_on){
					$is_valid = false;
					$user_message .= "The archived date is not valid.<br/>";
					$internal_message .= "Bad archived_on date!<br/>";
				}
			}

			//Throw a validation exception if something didn't pass
			if(!$is_valid){
				throw new ValidationException($user_message, "Asset Constructor - $internal_message");
			}


			
			$this->name = $name;
			$this->asset_tag = $asset_tag;
			$this->parent_id = $parent_id;
			$this->person_id = $person_id;
			$this->location_id = $location_id;
			$this->notes = $notes;
			$this->archived = $archived;
			$this->archived_on = $archived_on;

			parent::__construct($dbid);
		}

		
		public function getInfoArray(){
			$array = parent::getInfoArray();
			$array['name'] = $this->name;
			$array['parent_id'] = $this->parent_id;
			$array['person_id'] = $this->person_id;
			$array['location_id'] = $this->location_id;
			$array['asset_tag'] = $this->asset_tag;

			return $array;
		} 

		
		

		/***************************
			Getters and Setters
		***************************/
		//Even though most of the asset properties shouldn't need any extra logic, I'm making them protected, incase I want to extend that later
		//Also allows for validation on the setters


		/**
		* Getter for the Asset's name
		* 
		* @return string
		*/
		public function getName(){
			return $this->name;
		}

		/**
		* Setter for the Asset's name
		* 
		* @param string $name
		* @throws ValidationException
		*/
		public function setName($name){
			if(!preg_match(Model::$regex_string_short, $name)){
				throw new ValidationException("The name can only contain letters, numbers, spaces, dashes and periods<br/>", "VALIDATION ERROR - NAME: $name");
			}

			$this->name = $name;
		}

		/**
		* Getter for the Parent's DBID
		* 
		* @return int
		*/
		public function getParentID(){
			return $this->parent_id;
		}

		/**
		* Setter for the Parent's DBID
		* 
		* @param int $parent_id
		* @throws ValidationException
		*/
		public function setParentID($parent_id){
			if(!preg_match(Model::$regex_dbid, $parent_id)){
				throw new ValidationException("That parent asset is not valid<br/>", "VALIDATION ERROR - PARENT_ID: $parent_id");
			}

			$this->parent_id = $parent_id;
		}

		//Would like to add getParent and setParent so we don't have to work directly with ids

		/**
		* Getter for the User's DBID
		* 
		* @return int
		*/
		public function getPersonID(){
			return $this->person_id;
		}

		/**
		* Returns an object representing a person, or null if none is assigned
		* @return Person
		*/
		public function getPerson(){

			//If there is no person assigned, return null
			if($this->person_id == null){
				return null; 
			}
			$db = getDBConn(); //global function 
			$mapper = PersonMapper::GetInstance($db);
			$person = $mapper->find(array($this->person_id));
			$db->close();
			return $person[0]; //Because find returns an array
		}

		/**
		* Setter for the User's DBID
		* 
		* @param int $person_id
		* @throws ValidationException
		*/
		public function setPersonID($person_id){
			if($person_id !== null && !preg_match(Model::$regex_dbid, $person_id)){
				throw new ValidationException("That user is not valid<br/>", "VALIDATION ERROR - person_id: $person_id");
			}

			$this->person_id = $person_id;
		}

		/**
		* Returns an object representing a location, or null if none is assigned
		* @return Location
		*/
		public function getLocation(){

			//If there is no location assigned, return null
			if($this->location_id == null){
				return null; 
			}
			$db = getDBConn(); //global function 
			$mapper = LocationMapper::GetInstance($db);
			$location = $mapper->find(array($this->location_id));
			$db->close();
			return $location[0]; //Because find returns an array
		}


		/**
		* Getter for the Locations's DBID
		* 
		* @return int
		*/
		public function getLocationID(){
			return $this->location_id;
		}

		/**
		* Setter for the User's DBID
		* 
		* @param int $location_id
		* @throws ValidationException
		*/
		public function setLocationID($location_id){
			if($location_id !== null && !preg_match(Model::$regex_dbid, $location_id)){
				throw new ValidationException("That location is not valid<br/>", "VALIDATION ERROR - LOCATION_ID: $location_id");
			}

			$this->location_id = $location_id;
		}

		//Would like to add getLocation and setLocation


		/**
		* Getter for the Asset's tag (identification number)
		* 
		* @return int
		*/
		public function getAssetTag(){
			return $this->asset_tag;
		}

		/**
		* Setter for the Asset's tag
		* 
		* @param string $asset_tag
		* @throws ValidationException
		*/
		public function setAssetTag($asset_tag){
			if($asset_tag !== null && !preg_match(Model::$regex_asset_tag, $asset_tag)){
				throw new ValidationException("The asset tag can only contain letters, numbers, spaces, dashes and periods", "VALIDATION ERROR - ASSET_TAG: $asset_tag");
			}

			$this->asset_tag = $asset_tag;
		}



		/**
		* Getter for the Asset's notes
		* 
		* @return int
		*/
		public function getNotes(){
			return $this->notes;
		}

		/**
		* Setter for the Asset's notes
		* Will allow anything here for now, may want to limit this later, but I am using  prepared statements for DB interaction, so it /shouldn't/ be a problem
		* @param string $notes
		*/
		public function setNotes($notes){

			$this->notes = $notes;
		}



		/**
		* Getter for the Asset's archived status
		* 
		* @return boolean
		*/
		public function getIsArchived(){
			return $this->archived;
		}

		/**
		* Setter for the Asset's archived status
		* @param boolean $archived
		*/
		public function setIsArchived($archived){

			$this->archived = $archived;
		}




		/**
		* Getter for the Asset's archived date
		* 
		* @return Date
		*/
		public function getArchivedOn(){
			return $this->archived_on;
		}

		/**
		* Getter for the Asset's archived date as a sting
		* 
		* @return string
		*/
		public function getArchivedOnString(){
			return $this->archived_on->format("Y-m-d");
		}

		/**
		* Setter for the Asset's archived date
		* @param Date $archived_on
		*/
		public function setArchivedOn($archived_on){

			$this->archived_on = $archived_on;
		}

		
		
	}

	
?>