<?php
	/**
	* This file contains the software class
	*/
	

	/**
	* Models software, containing the licence, experation date, etc
	*/
	class Software extends Asset{
		/**
		* The serial code for the software
		* Can't really validate it against a regex
		* @var string
		*/
		private $serial;

		/**
		* The version of this software
		* @var string
		*/
		private $version;

		/**
		* The expiration date for this software
		* @var date
		*/
		private $expiration_date;

		/**
		* Constructor for the Software class
		* @param string $name
		* @param string $parent_id
		* @param string $person_id
		* @param string $location_id
		* @param string $asset_tag
		* @param string $serial 
		* @param string $version
		* @param date $expiration_date
		* @param int $dbid
		* @param boolean $archived
		* @param Date archivedOn
		* @throws Validation Exception
		*/
		public function __construct($name = '', 
									$parent_id = null, 
									$person_id = null, 
									$location_id = null, 
									$asset_tag = null, 
									$serial = null, 
									$version = null, 
									$expiration_date = null, 
									$notes = 'Some note',
									$dbid = null,
									$archived = false,
									$archived_on = null){

			$is_valid = true;
			$user_message = '';
			$internal_message = '';
			//Generating a dbid if it doesn't have one yet (creating not recreating)
			if($dbid === null){
				$dbid = Model::createModelID('software');
			}

			
			//Expiration expiration_date
			if($expiration_date !== null && !preg_match(Model::$regex_date, $expiration_date)){
				$is_valid = false;
				$user_message .= "That expiration date is invalid<br/>";
				$internal_message .= "Invalid expiration_date<br/>";
			}

			if(!$is_valid){
				throw new ValidationException($user_message, "VALIDATION EXCEPTION - Software Constructor - $internal_message");
			}

			$this->serial = $serial;
			$this->version = $version;
			$this->expiration_date = $expiration_date;

			parent::__construct($dbid, $name, $parent_id, $person_id, $location_id, $asset_tag, $notes, $archived, $archived_on);
		}

		public function getInfoArray(){
			$array = parent::getInfoArray();

			$array['serial'] = $this->serial;
			$array['version'] = $this->version;
			$array['expiration_date'] = $this->expiration_date;

			return $array;
		}

		public function getInfo(){
			return json_encode($this->getInfoArray());
		}


		public function getHTML(){
			$name = strtoupper($this->name); //Capitalizing the name
			$person = $this->getPerson(); //Getting the person object for its data
			$location = $this->getLocation(); //Getting the location object

			return "<form action='" . $GLOBALS['webroot'] . "/utils/data/software/addUpdateSoftware.php'>
				      <input type='hidden' name='id' value='$this->dbid'/>
				      

				      <div class='row'>
				            <!-- Name of the software -->
				            <div class='name col-md-12'>
				                  <h2 class='value'><div class='fa fa-floppy-o'></div> $name</h2>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-floppy-o'></div></div>
				                              <input type='text' class='form-control' name='name' placeholder='Name of software' value='$this->name'/>
				                        </div>
				                  </div>
				            </div><!-- End .name -->

				            
				      </div><!-- End .row -->
				      
				      <div class='row'>
				            <!-- Asset Tag -->
				            <div class='asset-tag col-md-4'>
				                  <div class='value'><div class='fa fa-tag'></div> <b>$this->asset_tag</b></div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-tag'></div></div>
				                              <input type='text' class='form-control' name='asset_tag' placeholder='Asset Tag' value='$this->asset_tag'/>
				                        </div>
				                  </div>
				            </div><!-- End .asset-tag -->

				            <!-- User -->
				            <div class='user col-md-4'>
				                  <div class='value'><div class='fa fa-user'></div>" . (($person != null) ?  " <a class='record' data-dbid='$this->person_id'><b> " . $person->getLast() . ", " . $person->getFirst() . " </b></a>": ' None') . "</div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-user'></div></div>
				                              <input type='text' class='form-control' name='employee_id' placeholder='Employee ID' value='" . (($person != null) ? $person->getEmployeeID() : '') . "'/>
				                        </div>
				                  </div>
				            </div><!-- End .user -->

				      <!-- Location -->
				            <div class='location col-md-4'>
				                  <div class='value'><div class='fa fa-building'></div> <b>" . (($location != null) ? " <a class='record' data-dbid='$this->location_id'><b> " . $location->getBuilding() . " " . $location->getNumber() . " </b></a>" : ' None' ) . "</b></div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-building'></div></div>
				                              <input type='text' class='form-control' name='location' placeholder='Location (SNR 338)' value='" . (($location != null) ? $location->getBuilding() . " " . $location->getNumber() : '') . "'/>
				                        </div>
				                  </div>
				            </div><!-- End .location -->
				      </div><!-- End .row -->

				      <div class='h-divider-dark value'></div>

				      <div class='row'>
				      		<!-- Serial Code -->
				            <div class='serial-code col-md-5'>
				                  <div class='value'>Serial: <b>$this->serial</b></div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>Serial</div>
				                              <input type='text' class='form-control' name='serial_code' placeholder='Serial Code' value='$this->serial'/>
				                        </div>
				                  </div>
				            </div><!-- End .serial-code -->

				      		<!-- Version -->
				            <div class='version col-md-3'>
				                  <div class='value'>Version: <b>$this->version</b></div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>Ver</div>
				                              <input type='text' class='form-control' name='version' placeholder='Version' value='$this->version'/>
				                        </div>
				                  </div>
				            </div><!-- End .version -->

				            <!-- Expiration Date -->
				            <div class='expiration-date col-md-4'>
				                  <div class='value'>Expiration Date: <b>" . (($this->expiration_date !== null && $this-> expiration_date != '') ? date("M/d/Y", $this->expiration_date) : 'None') . "</b></div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>Expires</div>
				                              <input type='date' class='form-control' name='expiration_date' value='$this->expiration_date'/>
				                        </div>
				                  </div>
				            </div><!-- End .expiration-date -->
				      </div><!-- End .row -->
				      
				</form>";
		}



		/***************************
			Getters and Setters
		***************************/

		/**
		* Returns the serial number for this software
		* @return string
		*/
		public function getSerial(){
			return $this->serial;
		}

		/**
		* Sets the serial for this software. Because there is no real way to validate serial numbers (there are so many variations), there is no validation run.
		* May be null
		* @param string $serial
		*/
		public function setSerial($serial){
			$this->serial = $serial;
		}

		/**
		* Returns the version for this software
		* @return string
		*/
		public function getVersion(){
			return $this->version;
		}

		/**
		* Sets the version for this software. Also not validated due to different variations
		* May be null
		* @param string $version
		*/
		public function setVersion($version){
			$this->version = $version;
		}

		/**
		* Returns the expiration date for this software's licence
		* @return date
		*/
		public function getExpirationDate(){
			return $this->expiration_date;
		}

		/**
		* Sets the expiration date for this software's licence. Must be in YYYY-MM-DD format
		* May be null
		* @param date $expiration_date
		* @throws ValidationError
		*/
		public function setExpirationDate($expiration_date){
			if($expiration_date !== null && !preg_match(Model::$regex_date, $expiration_date)){
				$is_valid = false;
				$user_message .= "That expiration date is invalid<br/>";
				$internal_message .= "Invalid expiration_date<br/>";
				throw new ValidationException($user_message, $internal_message);
			}

			$this->expiration_date = $expiration_date;
		}



		/**
		* This function will return the html used for adding new software
		*
		* @return string
		*/

		public static function GetNewHTML(){

			return "<form action='" . $GLOBALS['webroot'] . "/utils/data/software/addUpdateSoftware.php'>
				      <input type='hidden' name='id' value=''/>
				      

				      <div class='row'>
				            <!-- Name of the software -->
				            <div class='name col-md-12'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-floppy-o'></div></div>
				                              <input type='text' class='form-control' name='name' placeholder='Name of software' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .name -->

				            
				      </div><!-- End .row -->
				      
				      <div class='row'>
				            <!-- Asset Tag -->
				            <div class='asset-tag col-md-4'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-tag'></div></div>
				                              <input type='text' class='form-control' name='asset_tag' placeholder='Asset Tag' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .asset-tag -->

				            <!-- User -->
				            <div class='user col-md-4'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-user'></div></div>
				                              <input type='text' class='form-control' name='employee_id' placeholder='Employee ID' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .user -->

				      <!-- Location -->
				            <div class='location col-md-4'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'><div class='fa fa-building'></div></div>
				                              <input type='text' class='form-control' name='location' placeholder='Location (SNR 338)' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .location -->
				      </div><!-- End .row -->

				      <div class='h-divider-dark value'></div>

				      <div class='row'>
				      		<!-- Serial Code -->
				            <div class='serial-code col-md-5'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>Serial</div>
				                              <input type='text' class='form-control' name='serial_code' placeholder='Serial Code' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .serial-code -->

				      		<!-- Version -->
				            <div class='version col-md-3'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>Ver</div>
				                              <input type='text' class='form-control' name='version' placeholder='Version' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .version -->

				            <!-- Expiration Date -->
				            <div class='expiration-date col-md-4'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>Expires</div>
				                              <input type='date' class='form-control' name='expiration_date' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .expiration-date -->
				      </div><!-- End .row -->
				      
				</form>";
		}
	}
?>
