<?php
	/**
	* This file contians the Person Class
	*/
	
	
	/**
	* The person class is used to model entities (people) that have/use assets
	*/

	class Person extends Model{
		/**
		* First Name of the person
		* @var string
		*/
		private $first;

		/**
		* Last Name of the person
		* @var string
		*/
		private $last;

		/**
		* Person's email, can be null
		* @var string
		*/
		private $email;

		/**
		* Person's phone number, stored as an integer NOT a string, for consistancy in display, can be null
		* @var int
		*/
		private $phone;

		/**
		* Person's phone extention, may be null
		* @var int
		*/
		private $ext;

		/**
		* Employee ID for person
		* @var string
		*/
		private $employee_id;

		/**
		* Constructor for the Person class
		* @param int $dbid
		* @param string $employee_id
		* @param string $first
		* @param string $last
		* @param string $email
		* @param int $phone
		* @param int $ext
		* @throws ValidationException
		*/
		public function __construct($employee_id = null, $first = '', $last = '', $email = null, $phone = null, $ext = null, $dbid = null){
			$user_message = '';
			$internal_message = '';
			$is_valid = true;
			//Making a completeley new person, instead of recreaeting from the database
			if($dbid === null){
				$dbid = Model::createModelID('person');
			}
			//employee ID
			if($employee_id !== null && !preg_match(Model::$regex_employee_id, $employee_id)){
				$is_valid = false;
				$user_message .= "The Employee ID can only contain letters, numbers, -'s and spaces<br/>";
				$internal_message .= "Invalid employee_id<br/>";
			}

			//first name
			if(!preg_match(Model::$regex_person_name, $first)){
				$is_valid = false;
				$user_message .= "The first name can only contain letters, -'s, apostrophies and spaces<br/>";
				$internal_message .= "Invalid first<br/>";
			}

			//last name
			if(!preg_match(Model::$regex_person_name, $last)){
				$is_valid = false;
				$user_message .= "The last name can only contain letters, -'s, apostrophies and spaces<br/>";
				$internal_message .= "Invalid last<br/>";
			}

			//email
			if($email !== null && !preg_match(Model::$regex_email, $email)){
				$is_valid = false;
				$user_message .= "That email is invalid<br/>";
				$internal_message .= "Invalid email<br>";
			}

			//phone
			if($phone !== null){
				//Pulling only the digits, since there are an infinite number of ways to enter a phone number
				$phone_old = $phone;
				preg_match_all("/[\d]+/", $phone_old, $phone);
				$phone = implode($phone[0]);
				if(!preg_match(Model::$regex_phone, $phone)){
					echo "<br>" . $phone . "<br>";
					$is_valid = false;
					$user_message .= "That phone number is invalid, it must be 10 digits in length.<br>";
					$internal_message .= "Invalid phone<br/>";
				}
			}

			//ext
			if($ext !== null){
				//Pulling the digits, same as phone just no length requirement
				$ext_old = $ext;
				preg_match_all("/[\d]+/", $ext_old, $ext);
				$ext = implode($ext[0]);
				if(!preg_match(Model::$regex_ext, $ext)){
					$is_valid = false;
					$user_message = "That extention is not valid.<br>";
					$internal_message = "Invalid ext<br>";
				}
			}

			//Throw a validation exception if something didn't pass
			if(!$is_valid){
				throw new ValidationException($user_message, "VALIDATION EXCEPTION - Person Constructor - $internal_message");
			}

			$this->employee_id = $employee_id;
			$this->first = $first;
			$this->last = $last;
			$this->email = $email;
			$this->phone = ($phone !== null) ? filter_var($phone, FILTER_SANITIZE_NUMBER_INT) : null;
			$this->ext = ($ext !== null) ? filter_var($ext, FILTER_SANITIZE_NUMBER_INT) : null;

			parent::__construct($dbid);
		}




		public function getInfo(){
			return json_encode($this->getInfoArray());
		}



		public function getInfoArray(){
			$data = array();
			$data['dbid'] = $this->dbid;
			$data['first'] = $this->first;
			$data['last'] = $this->last;
			$data['employee_id'] = $this->employee_id;
			$data['email'] = $this->email;
			$data['phone'] = $this->phone;
			$data['ext'] = $this->ext;

			return $data;
		}

		public function getHTML(){
			return "<form action='" . $GLOBALS['webroot'] . "/utils/data/people/addUpdatePerson.php'>
					    <input type='hidden' name='id' value='$this->dbid'/>
					      

					    <div class='row'>
					            
					            <!-- Person's name -->
					            <div class='name col-md-12'>
					                  <h2 class='value'><div class='fa fa-user'></div> $this->last, $this->first</h2>
					                  <div class='form-group form-inline edit'>
					                        <div class='input-group'>
					                              <div class='input-group-addon'>Last</div>
					                              <input type='text' class='form-control' name='last' placeholder='Last Name' value='$this->last'/>
					                        </div>
					                        <div class='input-group'>
					                              <div class='input-group-addon'>First</div>
					                              <input type='text' class='form-control' name='first' placeholder='First Name' value='$this->first'/>
					                        </div>
					                  </div>
					            </div><!-- End .name -->

					    </div><!-- End .row -->
					      
					    <div class='row'>
					    	<!-- Person's employee id -->
					    	<div class='employee_id col-md-12'>
					    		<div class='value'>Employee ID: <b>$this->employee_id</b></div>
					    		<div class='form-group edit'>
					    			<div class='input-group'>
					    				<div class='input-group-addon'>ID</div>
					    				<input type='text' class='form-control' name='employee_id', placeholder='Employee ID' value='$this->employee_id'/>
					    			</div>
					    		</div>
					    	</div><!-- end .employee_id -->
					    </div><!-- End .row -->


					    <div class='row'>
					    	<!-- Person's email-->
					    	<div class='email col-md-5'>
					    		<div class='value'>Email: <b>$this->email</b></div>
					    		<div class='form-group edit'>
					    			<div class='input-group'>
					    				<div class='input-group-addon'>@</div>
					    				<input type='text' class='form-control' name='email', placeholder='Email' value='$this->email'/>
					    			</div>
					    		</div>
					    	</div><!-- end .email -->

					    	<!-- Person's phone-->
					    	<div class='email col-md-4'>
					    		<div class='value'>Phone: <b>$this->phone</b></div>
					    		<div class='form-group edit'>
					    			<div class='input-group'>
					    				<div class='input-group-addon'><div class='fa fa-phone'></div></div>
					    				<input type='text' class='form-control' name='phone', placeholder='Phone' value='$this->phone'/>
					    			</div>
					    		</div>
					    	</div><!-- end .email -->

					    	<!-- Person's extention-->
					    	<div class='email col-md-3'>
					    		<div class='value'>Ext.: <b>$this->ext</b></div>
					    		<div class='form-group edit'>
					    			<div class='input-group'>
					    				<div class='input-group-addon'>Ext.</div>
					    				<input type='text' class='form-control' name='ext', placeholder='Extention' value='$this->ext'/>
					    			</div>
					    		</div>
					    	</div><!-- end .email -->
					    </div><!-- End .row -->
					      
					</form>";
		}


		/***************************
			Getters and Setters
		***************************/

		/**
		* Getter for the Person's employee id
		* @return string
		*/
		public function getEmployeeID(){
			return $this->employee_id;
		}

		/**
		* Setter for the Person's employee id
		* @param string $employee_id
		* @throws ValidationException
		*/
		public function setEmployeeID($employee_id){
			if(!preg_match(Model::$regex_employee_id, $employee_id)){
				$user_message .= "The Employee ID can only contain letters, numbers, -'s and spaces<br/>";
				$internal_message .= "Invalid employee_id<br/>";
				throw new ValidationException($user_message, $internal_message);
			}

			$this->employee_id = $employee_id;
		}


		/**
		* Getter for the Person's first name
		* @return string
		*/
		public function getFirst(){
			return $this->first;
		}

		/**
		* Setter for the Person's first name
		* @param string $first
		* @throws ValidationException
		*/
		public function setFirst($first){
			if(!preg_match(Model::$regex_person_name, $first)){
				$user_message .= "The first name can only contain letters, -'s, apostrophies and spaces<br/>";
				$internal_message .= "Invalid first<br/>";
				throw new ValidationException($user_message, $internal_message);
			}

			$this->first = $first;
		}


		/**
		* Getter for the Person's last name
		* @return string
		*/
		public function getLast(){
			return $this->last;
		}

		/**
		* Setter for the Person's last name
		* @param string $last
		* @throws ValidationException
		*/
		public function setLast($last){
			if(!preg_match(Model::$regex_person_name, $last)){
				$user_message .= "The last name can only contain letters, -'s, apostrophies and spaces<br/>";
				$internal_message .= "Invalid last<br/>";
				throw new ValidationException($user_message, $internal_message);
			}

			$this->last = $last;
		}


		/**
		* Getter for the Person's email
		* @return string
		*/
		public function getEmail(){
			return $this->email;
		}

		/**
		* Setter for the Person's email
		* @param string $email
		* @throws ValidationException
		*/
		public function setEmail($email){
			if($email !== null && !preg_match(Model::$regex_email, $email)){
				$user_message .= "That email is invalid<br>";
				$internal_message .= "Invalid email<br/>";
				throw new ValidationException($user_message, $internal_message);
			}

			$this->email = $email;
		}


		/**
		* Getter for the Person's phone
		* @return string
		*/
		public function getPhone(){
			return $this->phone;
		}

		/**
		* Setter for the Person's phone
		* @param string $phone
		* @throws ValidationException
		*/
		public function setPhone($phone){
			if($phone !== null){
				//Stripping everything but the digits
				$phone_old = $phone;
				preg_match_all("/[\d]+/", $phone_old, $phone);
				$phone = implode($phone[0]);

				if(!preg_match(Model::$regex_phone, $phone)){
					$user_message .= "That phone is invalid, it must be 10 digits long<br>";
					$internal_message .= "Invalid phone<br/>";
					throw new ValidationException($user_message, $internal_message);
				}
			}

			$this->phone = $phone;
		}

		/**
		* Getter for the Person's extention
		* @return string
		*/
		public function getExt(){
			return $this->ext;
		}

		/**
		* Setter for the Person's extention
		* @param string $ext
		* @throws ValidationException
		*/
		public function setExt($ext){
			if($ext !== null){
				$ext = filter_var($ext, FILTER_SANITIZE_NUMBER_INT);
				if(!preg_match(Model::$regex_ext, $ext)){
					$user_message .= "That extention is invalid<br>";
					$internal_message .= "Invalid ext<br/>";
					throw new ValidationException($user_message, $internal_message);
				}
			}

			$this->ext = $ext;
		}




		/**
		* This function will return a blank edit form for creating new people
		*
		* @return string
		*/
		public static function getNewHTML(){
			return "<form action='" . $GLOBALS['webroot'] . "/utils/data/people/addUpdatePerson.php'>
					    <input type='hidden' name='id' value=''/>
					      

					    <div class='row'>
					            
					            <!-- Person's name -->
					            <div class='name col-md-12'>
					                  <div class='form-group form-inline edit'>
					                        <div class='input-group'>
					                              <div class='input-group-addon'>Last</div>
					                              <input type='text' class='form-control' name='last' placeholder='Last Name' value=''/>
					                        </div>
					                        <div class='input-group'>
					                              <div class='input-group-addon'>First</div>
					                              <input type='text' class='form-control' name='first' placeholder='First Name' value=''/>
					                        </div>
					                  </div>
					            </div><!-- End .name -->

					    </div><!-- End .row -->
					      
					    <div class='row'>
					    	<!-- Person's employee id -->
					    	<div class='employee_id col-md-12'>
					    		<div class='form-group edit'>
					    			<div class='input-group'>
					    				<div class='input-group-addon'>ID</div>
					    				<input type='text' class='form-control' name='employee_id', placeholder='Employee ID' value=''/>
					    			</div>
					    		</div>
					    	</div><!-- end .employee_id -->
					    </div><!-- End .row -->


					    <div class='row'>
					    	<!-- Person's email-->
					    	<div class='email col-md-5'>
					    		<div class='value'>Email: <b></b></div>
					    		<div class='form-group edit'>
					    			<div class='input-group'>
					    				<div class='input-group-addon'>@</div>
					    				<input type='text' class='form-control' name='email', placeholder='Email' value=''/>
					    			</div>
					    		</div>
					    	</div><!-- end .email -->

					    	<!-- Person's phone-->
					    	<div class='email col-md-4'>
					    		<div class='form-group edit'>
					    			<div class='input-group'>
					    				<div class='input-group-addon'><div class='fa fa-phone'></div></div>
					    				<input type='text' class='form-control' name='phone', placeholder='Phone' value=''/>
					    			</div>
					    		</div>
					    	</div><!-- end .email -->

					    	<!-- Person's extention-->
					    	<div class='email col-md-3'>
					    		<div class='form-group edit'>
					    			<div class='input-group'>
					    				<div class='input-group-addon'>Ext.</div>
					    				<input type='text' class='form-control' name='ext', placeholder='Extention' value=''/>
					    			</div>
					    		</div>
					    	</div><!-- end .email -->
					    </div><!-- End .row -->
					      
					</form>";
		}
	}
?>