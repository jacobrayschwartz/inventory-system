<?php 
	/**
	* A class file for PersonMapper
	*/

	
	/**
	* PersonMapper Class
	* Maps a Person to the database (or vice versa)
	* Implements I_DataMapper
	* Singleton
	*/
	class PersonMapper implements I_DataMapper{
		/**
		* The mysqli connection
		* @var mysqli
		*/
		private $db;

		/**
		* The instance of PersonMapper (Singleton Pattern)
		* @var PersonMapper
		*/
		private static $instance = null;

		/**
		* ModelFactory to build a computer
		* @var ModelFactory
		*/
		private $model_factory;

		/**
		* Private constructor for the PersonMapper, since  we are using the Singleton Pattern
		* @param mysqli $db
		*/
		private function __construct(mysqli $db){
			$this->db = $db;
			$this->model_factory = ModelFactory::GetInstance();
		}

		/**
		* Once nothing references this singleton, close the db connection
		*/
		/*public function __destruct(){
			if(isset($this->db) && $this->db !== null){
				$this->db->close();
			}
			unset($this->db);
		}*/

		/**
		* Returns an instance of the PersonMapper, or creates a new one if it hasen't been instantiated yet
		* @param mysqli $db
		*/
		public static function GetInstance(mysqli $db){
			if(PersonMapper::$instance === null){
				if($db == null){
					throw new DBException($GLOBALS['debug'] ? "Database can't be null!" : $GLOBALS['general_error_message']);
				}
				PersonMapper::$instance = new PersonMapper($db);
			}

			return PersonMapper::$instance;
		}

		/**
		* Making sure this object can't be cloned
		*/
		private function __clone(){
			//Making sure the object cant be cloned
		}

		/**
		* Making sure this object can't be unserialized
		*/
		private function __wakeup(){
			//Making sure the object can't be unserialized
		}


		//===================== CRUD!

		/**
		* Updates the mysql table-row representing the Person passed in
		* @param Person $person
		* @return boolean if update was successful
		*/
		public function update($person){
			$id = $person->getDBID();
			$employee_id = $person->getEmployeeID();
			$first = $person->getFirst();
			$last = $person->getLast();
			$email = $person->getEmail();
			$phone = $person->getPhone();
			$ext = $person->getExt();
			//var_dump($person);

			$stmt = $this->db->prepare("UPDATE people SET employee_id=?, first=?, last=?, email=?, phone=?, ext=? WHERE id=?");

			$stmt->bind_param('ssssiis', $employee_id, $first, $last, $email, $phone, $ext, $id);
			if(!$stmt->execute()){
				throw new DBException('Error Executing Statement!<br>' . $this->db->error);
			}
			unset($stmt);

			return true;
		}

		/**
		* Creates the mysql table-row that will represent the data passed in
		* @param Person $person
		* @return boolean if creation was successful
		*/
		public function create($person){
			$id = $person->getDBID();
			$employee_id = $person->getEmployeeID();
			$first = $person->getFirst();
			$last = $person->getLast();
			$email = $person->getEmail();
			$phone = $person->getPhone();
			$ext = $person->getExt();


			$stmt = $this->db->prepare("INSERT INTO people SET employee_id=?, first=?, last=?, email=?, phone=?, ext=?, id=?, model_type='person'");
			
			$stmt->bind_param('ssssiis', $employee_id, $first, $last, $email, $phone, $ext, $id);
			if(!$stmt->execute()){
				throw new DBException('Error Executing Statement!<br>' . $this->db->error);
			}
			unset($stmt);

			return true;
		}


		/**
		* Returns all people in the database
		* @return array $people
		* @throws DBException
		*/
		public function findAll(){
			$people = array();

			$rows = $this->db->query("SELECT id as 'dbid', first, last, email, employee_id, phone, ext, model_type FROM people");
			if(!$rows){
				//If nothing was returned
				throw new DBException("DBException: " . $this->db->error);
			}
			else{
				while($row = $rows->fetch_assoc()){
					$people[] = $this->model_factory->generateModel($row);
				}
			}
			return $people;
		}


		/**
		* Looks for the people in the database that match the ID's passed in
		* @param array $ids
		* @return array $people
		* @throws ValidationException
		* @throws DBException
		*/
		public function find(array $ids){
			$people = array();
			$id_string = '';
			for ($i = 0; $i < count($ids); $i++) {
				//Because I don't know how long the list of ids is going to be, I can't use a prepared statement, but the regex for a DBID should guard against SQL injection
				if(!preg_match(Model::$regex_dbid, $ids[$i])){
					throw new ValidationException('There was an issue with finding that model.','Invalid DBID: ' . $ids[$i]);
				}
				$id_string .= "'" . $ids[$i] . "'";
				if($i < count($ids) - 1){
					$id_string .= ', ';
				}
			}


			$rows = $this->db->query("SELECT id AS 'dbid', first, last, email, employee_id, phone, ext, model_type FROM people WHERE id IN ($id_string)");
			if(!$rows){
				//If nothing was returned
				throw new DBException("DBException: " . $this->db->error);
			}
			else if($rows->num_rows > 0){
				while($row = $rows->fetch_assoc()){
					$people[] = $this->model_factory->generateModel($row);
				}
			}
			return $people;
		}


		/**
		* Almost exactly like find, but looks for the employee id instead of the dbid
		* @param array $employee_ids
		* @return array 
		*/
		public function findByEmployeeID(array $employee_ids){
			$people = array();
			$id_string = '';
			for ($i = 0; $i < count($employee_ids); $i++) {
				if(!preg_match(Model::$regex_employee_id, $employee_ids[$i])){
					throw new ValidationException('Please enter a valid Employee ID.','Invalid Employee_id: ' . $employee_ids[$i]);
				}
				$id_string .= "'" . $employee_ids[$i] . "'";
				if($i < count($employee_ids) - 1){
					$id_string .= ', ';
				}
			}

			$rows = $this->db->query("SELECT id AS 'dbid', first, last, email, employee_id, phone, ext, model_type FROM people WHERE employee_id IN ($id_string)");
			if(!$rows){
				//If nothing was returned
				throw new DBException("DBException: " . $this->db->error);
			}
			else if($rows->num_rows > 0){
				while($row = $rows->fetch_assoc()){
					$people[] = $this->model_factory->generateModel($row);
				}
			}
			return $people;
		}

		/**
		* Deletes the row in the mysql database representing the person
		* @param int $id
		* @return boolean if deletion was successful
		* @throws ValidationException
		* @throws DBException
		*/
		public function delete($id){
			if(!preg_match(Model::$regex_dbid, $id)){
					throw new ValidationException('There was an issue with finding that model.','Invalid DBID: ' . $id);
			}

			
			$stmt = $this->db->prepare("DELETE FROM people WHERE id=?");
			$stmt->bind_param('s', $id);
			if(!$stmt->execute()){
				throw new DBException('Couldnt execute stmt: ' . $this->db->error());
			}
			$num = $stmt->affected_rows;
			unset($stmt);
			return ($num > 0);
		}
	}
?>