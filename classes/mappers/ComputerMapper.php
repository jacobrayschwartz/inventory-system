<?php 
	/**
	* A class file for ComputerMapper
	*/

	
	/**
	* Computer Mapper Class
	* Maps a Computer Asset to the database (or vice versa)
	* Implements I_DataMapper
	* Singleton
	*/
	class ComputerMapper implements I_DataMapper{
		/**
		* The mysqli connection
		* @var mysqli
		*/
		private $db;

		/**
		* The instance of ComputerMapper (Singleton Pattern)
		* @var ComputerMapper
		*/
		private static $instance = null;

		/**
		* ModelFactory to build a computer
		* @var ModelFactory
		*/
		private $model_factory;

		/**
		* Private constructor for the ComputerMapper, since  we are using the Singleton Pattern
		* @param mysqli $db
		*/
		private function __construct(mysqli $db){
			$this->db = $db;
			$this->model_factory = ModelFactory::GetInstance();
		}

		/**
		* Once nothing references this singleton, close the db connection
		*/
		/*public function __destruct(){
			if(isset($this->db) && $this->db !== null){
				$this->db->close();
			}
			unset($this->db);
		}*/

		/**
		* Returns an instance of the ComputerMapper, or creates a new one if it hasen't been instantiated yet
		* @param mysqli $db
		*/
		public static function GetInstance(mysqli $db){
			if(ComputerMapper::$instance === null){
				if($db == null){
					throw new DBException($GLOBALS['debug'] ? "Database can't be null!" : $GLOBALS['general_error_message']);
				}
				ComputerMapper::$instance = new ComputerMapper($db);
			}

			return ComputerMapper::$instance;
		}

		/**
		* Making sure this object can't be cloned
		*/
		private function __clone(){
			//Making sure the object cant be cloned
		}

		/**
		* Making sure this object can't be unserialized
		*/
		private function __wakeup(){
			//Making sure the object can't be unserialized
		}


		//===================== CRUD!

		/**
		* Updates the mysql table-row representing the Computer passed in
		* I would like to eventually use a stored procedure here, but it may couple the database and mapper too much
		* @param Computer $computer
		* @return boolean if update was successful
		*/
		public function update($computer){
			//Asset
			$id = $computer->getDBID();
			$name = $computer->getName();
			$parent_id = $computer->getParentID();
			$person_id = $computer->getPersonID();
			$location_id = $computer->getLocationID();
			$asset_tag = $computer->getAssetTag();
			$notes = $computer->getNotes();
			$archived = $computer->getIsArchived();
			$archived_on = $computer->getArchivedOnString();
			//Computer
			$disk_space = $computer->getDiskSpace();
			$memory = $computer->getMemory();
			$processor = $computer->getProcessor();
			$os = $computer->getOS();
			$type = $computer->getType();

			$asset_stmt = $this->db->prepare("UPDATE assets SET name=?, parent_id=?, person_id=?, location_id=?, asset_tag=?, notes=?, archived=?, archived_on=? WHERE id=?");
			$computer_stmt = $this->db->prepare("UPDATE computers SET disk_space=?, memory=?, processor=?, os=?, type=? WHERE asset_id=?");

			$asset_stmt->bind_param('ssssssiss', $name, $parent_id, $person_id, $location_id, $asset_tag, $notes, $archived, $archived_on, $id);
			if(!$asset_stmt->execute()){
				throw new DBException('Error Executing Update Statement!<br>' . $this->db->error);
			}
			unset($asset_stmt);

			
			$computer_stmt->bind_param('ddssss', $disk_space, $memory, $processor, $os, $type, $id);
			$computer_stmt->execute();
			if(!$computer_stmt->execute()){
				throw new DBException('Error Executing Update Statement!<br>' . $this->db->error);
			}
			unset($computer_stmt);

			return true;
		}

		/**
		* Creates the mysql table-row that will represent the data passed in
		* @param Computer $computer
		* @return boolean if creation was successful
		*/
		public function create($computer){
			//Asset
			$id = $computer->getDBID();
			$name = $computer->getName();
			$parent_id = $computer->getParentID();
			$person_id = $computer->getPersonID();
			$location_id = $computer->getLocationID();
			$asset_tag = $computer->getAssetTag();
			$notes = $computer->getNotes();
			$archived = $computer->getIsArchived();
			$archived_on = $computer->getArchivedOnString();
			//Computer
			$disk_space = $computer->getDiskSpace();
			$memory = $computer->getMemory();
			$processor = $computer->getProcessor();
			$os = $computer->getOS();
			$type = $computer->getType();

			$asset_stmt = $this->db->prepare("INSERT INTO assets SET name=?, parent_id=?, person_id=?, location_id=?, asset_tag=?, notes=?, id=?, model_type='computer', archived=?, archived_on=?");
			

			$asset_stmt->bind_param('sssssssis', $name, $parent_id, $person_id, $location_id, $asset_tag, $notes, $id, $archived, $archived_on);
			if(!$asset_stmt->execute()){
				throw new DBException('Error Executing Statement!<br>' . $this->db->error);
			}
			unset($asset_stmt);

			$computer_stmt = $this->db->prepare("INSERT INTO computers SET disk_space=?, memory=?, processor=?, os=?, type=?, asset_id=?");
			$computer_stmt->bind_param('ddssss', $disk_space, $memory, $processor, $os, $type, $id);
			if(!$computer_stmt->execute()){
				throw new DBException('Error Executing Create Statement!<br>' . $this->db->error);
			}
			unset($computer_stmt);

			return true;
		}


		/**
		* Returns all computers in the database
		* @return array $computers
		* @throws DBException
		*/
		public function findAll(){
			$computers = array();

			$rows = $this->db->query("SELECT assets.id AS 'dbid', 
											assets.name, 
											assets.parent_id, 
											assets.person_id, 
											assets.location_id, 
											assets.asset_tag, 
											assets.notes, 
											computers.disk_space, 
											computers.memory, 
											computers.processor, 
											computers.os, 
											computers.type, 
											assets.model_type
									FROM computers LEFT JOIN assets ON computers.asset_id=assets.id;");
			if(!$rows){
				//If nothing was returned
				throw new DBException("DBException: " . $this->db->error);
			}
			else{
				while($row = $rows->fetch_assoc()){
					$computers[] = $this->model_factory->generateModel($row);
				}
			}
			return $computers;
		}


		/**
		* Looks for the computers in the database that match the ID's passed in
		* @param array $ids
		* @return array $computers
		* @throws ValidationException
		* @throws DBException
		*/
		public function find(array $ids){
			$computers = array();
			$id_string = '';
			for ($i = 0; $i < count($ids); $i++) {
				//Because I don't know how long the list of ids is going to be, I can't use a prepared statement, but the regex for a DBID should guard against SQL injection
				if(!preg_match(Model::$regex_dbid, $ids[$i])){
					throw new ValidationException('There was an issue with finding that model.','Invalid DBID: ' . $ids[$i]);
				}
				$id_string .= "'" . $ids[$i] . "'";
				if($i < count($ids) - 1){
					$id_string .= ', ';
				}
			}


			$rows = $this->db->query("SELECT assets.id AS 'dbid', 
											assets.name, 
											assets.parent_id, 
											assets.person_id, 
											assets.location_id, 
											assets.asset_tag, 
											assets.notes, 
											computers.disk_space, 
											computers.memory, 
											computers.processor, 
											computers.os, 
											computers.type, 
											assets.model_type,
											current_loans.person_id as 'loan_person',
											current_loans.start_date as 'loan_start',
											current_loans.end_date as 'loan_end'
									FROM computers LEFT JOIN assets ON computers.asset_id=assets.id 
									LEFT OUTER JOIN 
										((SELECT loans.asset_id, loans.person_id, loans.start_date, loans.end_date
										FROM loans LEFT JOIN people ON loans.person_id=people.id
										WHERE turned_in IS NULL) as current_loans) ON assets.id=current_loans.asset_id
									WHERE assets.id IN ($id_string)");
			if(!$rows){
				//If nothing was returned
				throw new DBException("DBException: " . $this->db->error);
			}
			else{
				while($row = $rows->fetch_assoc()){
					$computers[] = $this->model_factory->generateModel($row);
				}
			}
			return $computers;
		}

		/**
		* Deletes the row in the mysql database representing the computer
		* @param int $id
		* @return boolean if deletion was successful
		* @throws ValidationException
		* @throws DBException
		*/
		public function delete($id){
			if(!preg_match(Model::$regex_dbid, $id)){
					throw new ValidationException('There was an issue with finding that model.','Invalid DBID: ' . $id);
			}

			//Because I used foreign keys to set the database, we only have to delete the asset record, the rest is taken care of by the database
			$stmt = $this->db->prepare("DELETE FROM assets WHERE id=?");
			$stmt->bind_param('s', $id);
			if(!$stmt->execute()){
				throw new DBException('Couldnt execute stmt: ' . $this->db->error());
			}
			$num = $stmt->affected_rows;
			unset($stmt);
			return ($num > 0);
		}
	}
?>