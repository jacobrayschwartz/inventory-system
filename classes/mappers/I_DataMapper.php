<?php 
	/**
	* A class file for I_DataMapper
	*/
	
	
	/**
	* DataMapper Interface
	* Requires a mysqli object
	* The objects that implement datamapper should be singletons
	*/
	interface I_DataMapper{
		/**
		* Returns an instance of the Mapper, or creates a new one if it hasen't been instantiated yet
		* @param mysqli $db
		* @return I_DataMapper
		*/
		public static function GetInstance(mysqli $db);
		/**
		* Updates the mysql table-row representing the object passed in
		* @param $object
		*/
		public function update($object);
		/**
		* Creates the mysql table-row that will represent the object passed in
		* @param $object
		*/
		public function create($object);
		/**
		* Returns all of the computers in the database
		* @return array $objects
		*/
		public function findAll();
		/**
		* Looks for the objects in the database that match the ID's passed in
		* @param array $ids
		* @return array $objects
		*/
		public function find(array $ids);
		/**
		* Deletes the row in the mysql database representing the object
		* @param int $id
		*/
		public function delete($id);
	}
?>