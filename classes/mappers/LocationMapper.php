<?php 
	/**
	* A class file for LocationMapper
	*/

	
	/**
	* LocationMapper Class
	* Maps a Location to the database (or vice versa)
	* Implements I_DataMapper
	* Singleton
	*/
	class LocationMapper implements I_DataMapper{
		/**
		* The mysqli connection
		* @var mysqli
		*/
		private $db;

		/**
		* The instance of LocationMapper (Singleton Pattern)
		* @var LocationMapper
		*/
		private static $instance = null;

		/**
		* ModelFactory to build a computer
		* @var ModelFactory
		*/
		private $model_factory;

		/**
		* Private constructor for the LocationMapper, since  we are using the Singleton Pattern
		* @param mysqli $db
		*/
		private function __construct(mysqli $db){
			$this->db = $db;
			$this->model_factory = ModelFactory::GetInstance();
		}

		/**
		* Once nothing references this singleton, close the db connection
		*/
		/*public function __destruct(){
			if(isset($this->db) && $this->db !== null){
				$this->db->close();
			}
			unset($this->db);
		}*/

		/**
		* Returns an instance of the LocationMapper, or creates a new one if it hasen't been instantiated yet
		* @param mysqli $db
		*/
		public static function GetInstance(mysqli $db){
			if(LocationMapper::$instance === null){
				if($db == null){
					throw new DBException($GLOBALS['debug'] ? "Database can't be null!" : $GLOBALS['general_error_message']);
				}
				LocationMapper::$instance = new LocationMapper($db);
			}

			return LocationMapper::$instance;
		}

		/**
		* Making sure this object can't be cloned
		*/
		private function __clone(){
			//Making sure the object cant be cloned
		}

		/**
		* Making sure this object can't be unserialized
		*/
		private function __wakeup(){
			//Making sure the object can't be unserialized
		}


		//===================== CRUD!

		/**
		* Updates the mysql table-row representing the Location passed in
		* @param Location $location
		* @return boolean if update was successful
		*/
		public function update($location){
			$dbid = $location->getDBID();
			$building = $location->getBuilding();
			$number = $location->getNumber();


			$stmt = $this->db->prepare("UPDATE locations SET building=?, `number`=? WHERE id=?");

			$stmt->bind_param('sis', $building, $number, $dbid);
			if(!$stmt->execute()){
				throw new DBException('Error Executing Statement!<br>' . $this->db->error);
			}
			unset($stmt);

			return true;
		}

		/**
		* Creates the mysql table-row that will represent the data passed in
		* @param Location $Location
		* @return boolean if creation was successful
		*/
		public function create($location){
			$dbid = $location->getDBID();
			$building = $location->getBuilding();
			$number = $location->getNumber();


			$stmt = $this->db->prepare("INSERT INTO locations SET building=?, `number`=?, id=?");
			
			$stmt->bind_param('sis', $building, $number, $dbid);
			if(!$stmt->execute()){
				throw new DBException('Error Executing Statement!<br>' . $this->db->error);
			}
			unset($stmt);

			return true;
		}


		/**
		* Returns all locations in the database
		* @return array $locations
		* @throws DBException
		*/
		public function findAll(){
			$locations = array();

			$rows = $this->db->query("SELECT id as 'dbid', building, `number`, model_type FROM locations");
			if(!$rows){
				//If nothing was returned
				throw new DBException("DBException: " . $this->db->error);
			}
			else{
				while($row = $rows->fetch_assoc()){
					$locations[] = $this->model_factory->generateModel($row);
				}
			}
			return $locations;
		}


		/**
		* Looks for the locations in the database that match the ID's passed in
		* @param array $ids
		* @return array $locations
		* @throws ValidationException
		* @throws DBException
		*/
		public function find(array $ids){
			$locations = array();
			$id_string = '';
			for ($i = 0; $i < count($ids); $i++) {
				//Because I don't know how long the list of ids is going to be, I can't use a prepared statement, but the regex for a DBID should guard against SQL injection
				if(!preg_match(Model::$regex_dbid, $ids[$i])){
					throw new ValidationException('There was an issue with finding that model.','Invalid DBID: ' . $ids[$i]);
				}
				$id_string .= "'" . $ids[$i] . "'";
				if($i < count($ids) - 1){
					$id_string .= ', ';
				}
			}


			$rows = $this->db->query("SELECT id AS 'dbid', building, `number`, model_type FROM locations WHERE id IN ($id_string)");
			if(!$rows){
				//If nothing was returned
				throw new DBException("DBException: " . $this->db->error);
			}
			else if($rows->num_rows > 0){
				while($row = $rows->fetch_assoc()){
					$locations[] = $this->model_factory->generateModel($row);
				}
			}
			return $locations;
		}


		/**
		* Almost exactly like find, but looks for the name of the building instead of the dbid (should be in the format of SNR335, the input is trimmed before the database is queried so spaces will not matter)
		* @param array $name
		* @return array 
		*/
		public function findByName($names){
			$locations = array();
			$name_string = '';
			for ($i = 0; $i < count($names); $i++) {
				//Removing all spaces from name
				$names[$i] = strtoupper(preg_replace('/[\s]+/', '', $names[$i]));

				if(!preg_match(Model::$regex_string_short, $names[$i])){
					throw new ValidationException('There was an issue with finding that model.','Invalid location name: ' . $names[$i]);
				}
				$name_string .= "'" . $names[$i] . "'";
				if($i < count($names) - 1){
					$name_string .= ', ';
				}
			}

			$rows = $this->db->query("SELECT id AS 'dbid', building, `number`, model_type FROM locations WHERE CONCAT(building, `number`) IN ($name_string)");
			if(!$rows){
				//If nothing was returned
				throw new DBException("DBException: " . $this->db->error);
			}
			else if($rows->num_rows > 0){
				while($row = $rows->fetch_assoc()){
					$locations[] = $this->model_factory->generateModel($row);
				}
			}
			return $locations;
		}

		/**
		* Deletes the row in the mysql database representing the Location
		* @param int $id
		* @return boolean if deletion was successful
		* @throws ValidationException
		* @throws DBException
		*/
		public function delete($id){
			if(!preg_match(Model::$regex_dbid, $id)){
					throw new ValidationException('There was an issue with finding that model.','Invalid DBID: ' . $id);
			}

			
			$stmt = $this->db->prepare("DELETE FROM locations WHERE id=?");
			$stmt->bind_param('s', $id);
			if(!$stmt->execute()){
				throw new DBException('Couldnt execute stmt: ' . $this->db->error());
			}
			$num = $stmt->affected_rows;
			unset($stmt);
			return ($num > 0);
		}
	}
?>