<?php
	/**
	* This file contains the I_ModelFactory Interface
	*/
	
	/**
	* Interface for the factories in the inventory
	* Each factory will be designated a specific group (Asset, User, etc).
	*/

	interface I_ModelFactory{
		/**
		* Returns an instance of the Factory, or creates a new one if it hasen't been instantiated yet
		* @return I_ModelFactory
		*/
		public static function GetInstance();

		/**
		* Returns the correct model acording to the given data
		* @param array $data
		* @return Model
		*/
		public function generateModel($data);
	}
?>