<?php
	/**
	* This file contains the ModelFactory
	*/
	
	
	/**
	* This factory takes in an associative array as input for the new object.
	* These arrays should come from a MySQL row, containing the type of the model.
	* Even though this is an extremeley simple object, I want to use the singleton pattern incase it needs to grow
	*/
	class ModelFactory implements I_ModelFactory{
		
		private static $instance;

		private function __construct(){
			//So far nothing
		}

		/**
		* Returns an instance of the ModelFactory (Singleton)
		* @return ModelFactory
		*/
		public static function GetInstance(){
			if(ModelFactory::$instance === null){
				ModelFactory::$instance = new ModelFactory();
			}
			return ModelFactory::$instance;
		}



		/**
		* Creates a concrete asset (Computer) based on the type stored in the array
		* @param string $data
		* @return Asset
		* @throws Exception if type does not exist
		*/
		public function generateModel($data){
			switch ($data['model_type']) {
				case 'computer':
					return $this->generateComputer($data);
					break;
				case 'location':
					return $this->generateLocation($data);
					break;
				case 'person':
					return $this->generatePerson($data);
					break;
				case 'software':
					return $this->generateSoftware($data);
					break;
				default:
					throw new Exception('That model type was not found!');
					break;
			}
		}

		private function generateComputer($data){
			return new Computer($data['name'], $data['parent_id'], $data['person_id'], $data['location_id'], $data['asset_tag'], $data['disk_space'], $data['memory'], $data['processor'], $data['os'], $data['type'], $data['notes'], $data['dbid']);
		}

		private function generateSoftware($data){
			return new Software ($data['name'], $data['parent_id'], $data['person_id'], $data['location_id'], $data['asset_tag'], $data['serial_code'], $data['version'], $data['expiration_date'], $data['notes'], $data['dbid']);
		}

		private function generatePerson($data){
			return new Person($data['employee_id'], $data['first'], $data['last'], $data['email'], $data['phone'], $data['ext'], $data['dbid']);
		}

		private function generateLocation($data){
			return new Location($data['dbid'], $data['building'], $data['number']);
		}
	}
?>