<?php
	/**
	* This file contains the Location class
	*/
	

	/**
	* The Location Class
	* Represents a location (room)
	*/

	class Location extends Model{

		/**
		* Bulding abbrev (or other prefix)
		* @var string
		*/
		private $building;

		/**
		* Room number
		* @var int
		*/
		private $number;

		/**
		* Constructor for location
		* @param string $dbid
		* @param string $building
		* @param int $room
		*/
		public function __construct($dbid = null, $building = '', $number = ''){
			//Creating a database ID, if one was not supplied 
			if($dbid === null){
				$dbid = Model::CreateModelID('location');
			}

			$building = strtoupper($building);
			/*
			Error checking
			Want to return all errors in the message, so using a boolean value to mark if there is an issue
			*/
			$is_valid = true;
			$user_message = '';
			$internal_message = '';

			if(!preg_match(Model::$regex_building, $building)){
				$is_valid = false;
				$user_message = 'The building/prefix should contain letters only.<br>';
				$internal_message = "Invalid building: $building<br>";
			}

			if(!preg_match(Model::$regex_pos_integer, $number)){
				$is_valid = false;
				$user_message = 'The room number should contain digits only.<br>';
				$internal_message = "Invalid number: $number<br>";
			}

			if(!$is_valid){
				throw new ValidationException($user_message, "VALIDATION EXCEPTION - Location Constructor - $internal_message");
			}

			//Assigning the properties
			$this->building = $building;
			$this->number = $number;
			parent::__construct($dbid);
		}


		public function getInfo(){
			return json_encode($this->getInfoArray());
		}

		
		public function getInfoArray(){
			$array = parent::getInfoArray();
			$array['building'] = $this->building;
			$array['number'] = $this->number;

			return $array;
		}

		
		public function getHTML(){
			return "
				<form action='" . $GLOBALS['webroot'] . "/utils/data/locations/addUpdateLocation.php'>
				    <input type='hidden' name='id' value='$this->dbid'/>
				      

				    <div class='row'>
				            <!-- Building -->
				            <div class='building col-md-6'>
				            	  <div class='value'>Building: <b>$this->building</b></div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>Building</div>
				                              <input type='text' class='form-control' name='building' placeholder='Building (SNR)' value='$this->building'/>
				                        </div>
				                  </div>
				            </div><!-- End .building -->

				            <!-- Room -->
				            <div class='room col-md-6'>
				            	  <div class='value'>Room: <b>$this->number</b></div>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>Room</div>
				                              <input type='text' class='form-control' name='number' placeholder='Room Number' value='$this->number'/>
				                        </div>
				                  </div>
				            </div><!-- End .Room -->

				    </div><!-- End .row -->
				</form>
			";
		}


		/***************************
			Getters and Setters
		***************************/

		/**
		* Getter for the building/prefix
		* @return string
		*/
		public function getBuilding(){
			return $this->building;
		}

		/**
		* Setter for the building/prefix
		* @param string $building
		* @throws ValidationException
		*/
		public function setBuilding($building){
			$building = strtoupper($building);
			if(!preg_match(Model::$regex_string_short, $building)){
				throw new ValidationException("The building/prefix should contain only letters<br/>", "VALIDATION ERROR - BUILDING: $building - CLASS: " . get_class($this));
			}

			$this->building = $building;
		}

		/**
		* Getter for the room number
		* @return int
		*/
		public function getNumber(){
			return $this->number;
		}

		/**
		* Setter for the room number
		* @param int $number
		* @throws ValidationException
		*/
		public function setParentID($number){
			if(!preg_match(Model::$regex_pos_integer, $number)){
				throw new ValidationException("The room number should contain numbers only<br/>", "VALIDATION ERROR - NNUMBER: $number - CLASS: " . get_class($this));
			}

			$this->number = $number;
		}


		public static function GetNewHTML(){
			return "
				<form action='" . $GLOBALS['webroot'] . "/utils/data/locations/addUpdateLocation.php'>
				    <input type='hidden' name='id' value=''/>
				      

				    <div class='row'>
				            <!-- Building -->
				            <div class='building col-md-6'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>Building</div>
				                              <input type='text' class='form-control' name='building' placeholder='Building (SNR)' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .building -->

				            <!-- Room -->
				            <div class='room col-md-6'>
				                  <div class='form-group edit'>
				                        <div class='input-group'>
				                              <div class='input-group-addon'>Room</div>
				                              <input type='text' class='form-control' name='number' placeholder='Room Number' value=''/>
				                        </div>
				                  </div>
				            </div><!-- End .Room -->

				    </div><!-- End .row -->
				</form>
			";
		}
	}	
?>