<?php
	/**
	* This file contains the Model class
	*/
	

	/**
	* The Model Class
	* An abstract class for any model in the database (Assets, Users, Locations, etc)
	*/
	abstract class Model{
		//Regexes used for validation
		/**
		* Regex to match a database ID
		* @var string
		*/
		public static $regex_dbid = "/^[0-9a-zA-Z\.\-]+$/"; //Using the auto_increment feature in MySQL for the ID of an object, as opposed to UUID
		
		/**
		* Regex to match a short simple string up to 254 chars, can't be blank
		* @var string
		*/
		public static $regex_string_short = "/^[A-Za-z0-9\s\,\-\.\_]{1,254}$/"; //Alpha numeric, allowing spaces, -'s, periods up to 254 chars
		
		/**
		* Regex to match an asset tag
		* @var string
		*/
		public static $regex_asset_tag = "/^[A-Za-z0-9\s\,\-\.\_]{1,254}$/";
		
		/**
		* Regex to match a positive integer or decimal
		* @var string
		*/
		public static $regex_pos_number = "/^[0-9]*\.?[0-9]+$/";

		/**
		* Regex to match a positive integer only
		* @var string
		*/
		public static $regex_pos_integer = "/^[0-9]+$/";

		/**
		* Regex to match a person's name
		* @var string
		*/
		public static $regex_person_name = "/^[A-Za-z\.\'\s\-]+$/";

		/**
		* Regex to match a person's employee id
		* @var string
		*/
		public static $regex_employee_id = "/^[A-Za-z0-9\.\s\-]+$/";

		/**
		* Regex to match a person's employee id
		* @var string
		* @source http://www.regular-expressions.info/dates.html
		*/
		public static $regex_date = "/^(19|20)\d\d[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])$/";

		/**
		* Regex to match an email
		* @var string
		*/
		public static $regex_email = "/^[a-zA-Z0-9\.\-\_]+@[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)*\.[a-zA-Z]+$/";

		/**
		* Regex to match a sanitized phone US number (for now, just digits, for consistant formatting)
		* @var string
		*/
		public static $regex_phone = "/^[0-9]{10}$/";

		/**
		* Regex to match a sanitized extention (for now, just digits, for consistant formatting)
		* @var string
		*/
		public static $regex_ext = "/^[0-9]+$/";

		/**
		* Regex to match a building/prefix abbreviation for location
		* @var string
		*/
		public static $regex_building = "/^[A-Z]{1,20}/";

		/**
		* Database id for this asset, not to be confused with an asset tag
		* @var string
		*/
		protected $dbid;

		/**
		* Constructor for Model
		* @param string $dbid
		* @throws ValidationException
		*/
		protected function __construct($dbid){
			
			if(!preg_match(self::$regex_dbid, $dbid)){
				$is_valid = false;
				$user_message = "There was an issue with that object<br/>";
				$internal_message = "Invalid dbid<br/>";
				throw new ValidationException($user_message, "VALIDATION EXCEPTION - Model Constructor - $internal_message");
			}

			$this->dbid = $dbid;
		}

		/**
		* Returns a json object representing the object
		* @return JSON
		*/
		abstract public function getInfo();

		

		/**
		* Returns an associative array. Because each asset type will have different fields, it needs to define its own getInfo() and getInfoArray()
		* @return array
		*/
		public function getInfoArray(){
			$array = array();
			$array['dbid'] = $this->dbid;

			return $array;
		}

		/**
		* Getter for the DBID
		* There is no setter for the DBID, since we should never change the ID in the database
		* @return string
		*/
		public function getDBID(){
			return $this->dbid;
		}

		/**
		* Returns the HTML to be used in the detail vew on the website
		* This feels a bit tightly coupled, but it seems to be the best solution for this display
		* The content returned should go inside of a modal-body div
		* @return string
		*/
		public abstract function getHTML();


		/**
		* Generate a unique ID for the database with a prefix
		* Using with model entites (Assets, Users, etc)
		*/
		public static function CreateModelID($prefix){
			return uniqid($prefix, true);
		}

		
	}
?>