//Getting the data
function updateData(){
	
	var gl = getLoans();
	gl.done(function(response){
		response = JSON.parse(response);
		if(response.error){
			var $alert = generateAlert(response.data, 'danger');
			$('#content-pane').prepend($alert);
		}
		else{

			loanList = response.data;
		}
	});

	//Getting the data from computers
	var gc = getComputers();
	gc.done(function(response){
		response = JSON.parse(response);
		if(response.error){
			var $alert = generateAlert(response.data, 'danger');
			$('#content-pane').prepend($alert);
		}
		else{
			filteredComputers = allComputers = response.data;
		}
	});

	var gl = getLocations();
	gl.done(function(response){
		response = JSON.parse(response);
		if(response.error){
			var $alert = generateAlert(response.data, 'danger');
			$('#content-pane').prepend($alert);
		}
		else{
			filteredLocations = allLocations = response.data;
		}
	});


	var gp = getPeople();
	gp.done(function(response){
		response = JSON.parse(response);
		if(response.error){
			var $alert = generateAlert(response.data, 'danger');
			$('#content-pane').prepend($alert);
		}
		else{
			filteredPeople = allPeople = response.data;
		}
	});

	var gs = getSoftware();
	gs.done(function(response){
		response = JSON.parse(response);
		if(response.error){
			var $alert = generateAlert(response.data, 'danger');
			$('#content-pane').prepend($alert);
		}
		else{
			filteredSoftware = software = response.data;
		}
	});

	$.when(gc, gp, gl, gs).done(function(){
		curList = $('#view-selection option:selected').attr('value');
		doListing(curList);
	});
}


/*
When the user changes the view selection, change the listing being displayed
*/
$('#view-selection').change(function(e){
	e.preventDefault();
	var selection = $('#view-selection option:selected').attr('value');
	doListing(selection);
});


function doListing(dataSetChoice){
	var tableHeadingHTML;
	var tableDataHTML;

	switch (dataSetChoice){
		case 'computers':
			$('#listing-panel .panel-heading').text('Computers');
			tableHeadingHTML = "<tr><th>Name</th><th>Type</th><th>Asset Tag</th><th>Location</th><th>User</th></tr>";
			tableDataHTML = makeComputerDataListings();
			break;
		case 'locations':
			$('#listing-panel .panel-heading').text('Locations');
			tableHeadingHTML = "<tr><th>Building</th><th>Number</th></tr>";
			tableDataHTML = makeLocationDataListings();
			break;
		case 'people':
			$('#listing-panel .panel-heading').text('People');
			tableHeadingHTML = "<tr><th>Last</th><th>First</th><th>Employee ID</th><th>Email</th><th>Phone</th><th>Ext.</th></tr>";
			tableDataHTML = makePersonDataListings();
			break;
		case 'software':
			$('#listing-panel .panel-heading').text('Software');
			tableHeadingHTML = "<tr><th>Name</th><th>Version</th><th>Asset Tag</th><th>User</th></tr>";
			tableDataHTML = makeSoftwareDataListings();
			break;
		default:
			$('#listing-panel .panel-heading').text('Error');
			tableHeadingHTML = '';
			tableDataHTML = '';
	}
	$('#listing-panel thead').html(tableHeadingHTML);
	$('#listing-panel tbody').html(tableDataHTML);
}


//Generating the HTML listings based on the global variable filteredComputers
function makeComputerDataListings(){
	var html = '';
	numRows = filteredComputers.length;
	for(var i = 0; i < numRows; i++){
		var computer = filteredComputers[i];

		//Finding the person data to match with this computer
		var personData;
		if(computer.person_id != null){
			for(var p = 0; p < allPeople.length; p++){
				if(allPeople[p].dbid === computer.person_id){
					personData = allPeople[p];
					break;
				}
			}
		}

		//Finding the location data to match with this computer
		var locationData;
		if(computer.location_id != null){
			for(var l = 0; l < allLocations.length; l++){
				if(allLocations[l].dbid === computer.location_id){
					locationData = allLocations[l];
					break;
				}
			}
		}

		var status = '';
		for(var s = 0; s < loanList.length; s++){
			if(loanList[s].asset_id == computer.dbid){
				status = loanList[s].status;
				status = status + ((status == "overdue") ? " danger" : " info");
				break;
			}
		}

		
		
		html += "<tr class='record " + status + "' data-model='computer' data-dbid='" + computer.dbid + "'>";
		
		html += "<td class='name'>" + computer.name + "</td>";
		html += "<td class='type'>" + computer.type + "</td>";
		html += "<td class='asset_tag'>" + computer.asset_tag + "</td>"
		html += "<td class='location'>" + ((computer.location_id != null) ? locationData['building'] + " " + locationData['number'] : '') + "</td>";
		html += "<td class='person'>" + ((computer.person_id != null) ?  personData['last'] + ", " + personData['first']  : 'None') + "</td>";
		html += "</tr>";
	}

	return html;
}


//Generating the HTML listings from the filteredLocations
function makeLocationDataListings(){
	var html = '';
	numRows = filteredLocations.length;
	for(var i = 0; i < numRows; i++){
		var location = filteredLocations[i];


		html += "<tr class='record' data-model='location' data-dbid='" + location.dbid + "'>";
		html += "<td class='building'>" + location.building + "</td>";
		html += "<td class='number'>" + location.number + "</td>";
		html += "</tr>";
	}

	return html;
}


//Generating the HTML listings from the filteredPeople
function makePersonDataListings(){
	var html = '';
	numRows = filteredPeople.length;
	for(var i = 0; i < numRows; i++){
		var person = filteredPeople[i];


		html += "<tr class='record' data-model='person' data-dbid='" + person.dbid + "'>";
		html += "<td class='last'>" + person.last + "</td>";
		html += "<td class='first'>" + person.first + "</td>";
		html += "<td class='employee_id'>" + person.employee_id + "</td>";
		html += "<td class='email'>" + ((person.email != null) ? person.email : '') + "</td>";
		html += "<td class='phone'>" + ((person.phone != null) ? person.phone : '') + "</td>";
		html += "<td class='ext'>" + ((person.ext != null) ? person.ext : '') + "</td>";
		html += "</tr>";
	}

	return html;
}


//Generating the HTML listings from the filteredSoftware
function makeSoftwareDataListings(){
	var html = '';
	numRows = filteredSoftware.length;
	for(var i = 0; i < numRows; i++){
		var software = filteredSoftware[i];


		//Finding the person data to match with this software
		var personData;
		if(software.person_id != null){
			for(var p = 0; p < allPeople.length; p++){
				if(allPeople[p].dbid === software.person_id){
					personData = allPeople[p];
					break;
				}
			}
		}

				

		html += "<tr class='record' data-model='software' data-dbid='" + software.dbid + "'>";
		html += "<td class='name'>" + software.name + "</td>";
		html += "<td class='version'>" + ((software.version) ? software.version : '') + "</td>";
		html += "<td class='asset_tag'>" + ((software.asset_tag) ? software.asset_tag : 'None') + "</td>";
		html += "<td class='person'>" + ((software.person_id != null) ?  personData['last'] + ", " + personData['first']  : 'None') + "</td>";
		html += "</tr>";
	}

	return html;
}