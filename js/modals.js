/*
This file contains the javascripts that control the modals used in the website 
(not to be confused with the modal script from bootstrap)
*/


/*
When a record is clicked, bring up the details for that record in a modal
Use the data-model attribute to choose the correct modal
*/
$('body').on('click','.record',function(e){
	e.preventDefault();
	var id = [$(this).attr('data-dbid')];
	//Destroying all alerts
	$('.alert').alert('close');

	//Getting the HTML from the model
	$.post(webroot + '/utils/data/findModels.php',
		{ids: id, html: true},
		function(response){
			response = JSON.parse(response);
			if(!response.error){
				$('#details-modal .modal-body').html(response.data[0]['html']);

				//Hiding the edit fields
				$('#details-modal .value').show();
				$('#details-modal .edit').hide();
				$('#details-modal').modal();
			}
			else{//If there was an issue
				var a = generateAlert(response.data, 'danger');
				a.hide();
				$('#content-pane').prepend(a);
				a.slideDown();
			}
		});
	
});


//User wants to edit model
$('#details-modal').on('click','.show-edit',function(e){
	e.preventDefault();
	var $this = $(this);
	$this.parents('.modal').find('.value').hide();
	$this.parents('.modal').find('.edit').show();
});


//User cancels editing the model
$('#details-modal').on('click','.cancel-edit',function(e){
	e.preventDefault();
	//Destroying all alerts
	$('.alert').alert('close');
	var $this = $(this);
	$this.parents('.modal').find('.edit').hide();
	$this.parents('.modal').find('.value').show();
});

//User submits the new data for the model
$('.modal.model').on('click','.submit-form',function(e){
	e.preventDefault();


	//Destroying all alerts
	$('.alert').alert('close');

	var $this = $(this);
	var $modal = $this.parents('.modal');
	var $form = $modal.find('form');
	var dbid = $form.find('input[name=id]').attr('value');

	$.post($form.attr('action'),
		$form.serialize(),
		function(response){
			response = JSON.parse(response);
			if(response.error){
				var a = generateAlert(response.data, 'danger');
				a.hide();
				$modal.find('.modal-body').prepend(a);
				a.slideDown();
			}
			//If the update went ok
			else{
				//Repopulate the information
				updateData();
				
				if(!dbid){
					//If there is no id, ex creating a model, hide the modal
					$modal.modal('hide');
				}
				//Getting the HTML from the model
				$.post(webroot + '/utils/data/findModels.php',
					{ids: [dbid], html: true},
					function(updatedResponse){
						updatedResponse = JSON.parse(updatedResponse);
						if(!updatedResponse.error){
							//$modal = $('#details-modal');
							$modal.find('.modal-body').html(updatedResponse.data[0]['html']);

							//Hiding the edit fields
							$modal.find('.value').show();
							$modal.find('.edit').hide();
							//$modal.modal();

							//Displaying message from addUpdate
							var a = generateAlert(response.data, 'success');
							a.hide();
							$modal.find('.modal-body').prepend(a);
							a.slideDown();



						}
						else{//If there was an issue with pullting the info
							var a = generateAlert(updatedResponse.data, 'danger');
							a.hide();
							$modal.find('.modal-body').prepend(a);
							a.slideDown();
						}
					});
			}
		});
});


//User cancels adding the model
$('#new-form-modal').on('click','.cancel',function(e){
	e.preventDefault();
	//Destroying all alerts
	$('.alert').alert('close');
	$('.modal').modal('hide');
});