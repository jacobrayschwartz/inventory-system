//This file contains the code for pulling/pushing the data for the location models

/*
Trivial pull data, will return a jqXHR object
*/
function getLocations(){
	return $.post(webroot + '/utils/data/locations/getLocations.php');
}